package os.resources;

import os.core.*;
import os.rm.*;
import java.util.ArrayList;

public class ExternalReaderEndResource extends Resource {
    private ArrayList<Word> readData;
    private Word readSingleData;

    public ExternalReaderEndResource() {
        this.externalName = "ExternalReaderEndResource";
        this.reusable = false;
    }

    public ExternalReaderEndResource(ArrayList<Word> readData) {
        this();
        this.readData = readData;
    }

    public ExternalReaderEndResource(Word singleData) {
        this();
        this.readSingleData = singleData;
    }

    public ArrayList<Word> getReadData() {
        return this.readData;
    }

    public Word getReadSingleData() {
        return this.readSingleData;
    }

    @Override
    public void distribute() {
        this.distributeToReceivers();
    }
}
