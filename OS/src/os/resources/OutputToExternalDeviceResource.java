package os.resources;

import java.util.ArrayList;

import os.core.*;
import os.rm.Word;
import os.ui.UserInterface;

public class OutputToExternalDeviceResource extends Resource {
    private ArrayList<Word> output;

    public OutputToExternalDeviceResource() {
        this.externalName = "OutputToExternalDeviceResource";
        this.reusable = false;
    }

    public OutputToExternalDeviceResource(ArrayList<Word> output) {
        this();
        UserInterface.log("[OutputToExternalDeviceResource] Output is: " + output,3);
        this.output = output;
    }

    public boolean isOutput() {
        return this.output != null;
    }

    public ArrayList<Word> getOutput() {
        return this.output;
    }

}
