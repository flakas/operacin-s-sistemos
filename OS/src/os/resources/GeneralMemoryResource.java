package os.resources;

import os.core.*;

public class GeneralMemoryResource extends Resource {
    public GeneralMemoryResource() {
        this.externalName = "GeneralMemoryResource";
        this.reusable = true;
    }

}
