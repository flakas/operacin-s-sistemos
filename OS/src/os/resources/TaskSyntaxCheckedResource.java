package os.resources;

import java.util.ArrayList;

import os.core.*;
import os.rm.Word;

public class TaskSyntaxCheckedResource extends Resource {
    private ArrayList<Word> program;

    public TaskSyntaxCheckedResource() {
        this.externalName = "TaskSyntaxCheckedResource";
        this.reusable = false;
    }

    public TaskSyntaxCheckedResource(ArrayList<Word> program) {
        this();
        this.program = program;
    }

    public ArrayList<Word> getProgram() {
        return this.program;
    }

}
