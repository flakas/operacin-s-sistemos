package os.resources;

import os.core.*;

public class NonExistingResource extends Resource {
    public NonExistingResource() {
        this.externalName = "NonExistingResource";
        this.reusable = false;
    }

    @Override
    public void release(Resource element) {
    }

    @Override
    public void distribute() {
    }
}
