package os.resources;

import os.core.*;
import os.rm.Word;

public class InputFromGeneralMemoryResource extends Resource {
    private Word address;

    public InputFromGeneralMemoryResource() {
        this.externalName = "InputFromGeneralMemoryResource";
        this.reusable = false;
    }

    public InputFromGeneralMemoryResource(Word address) {
        this();
        this.address = address;
    }

    public Word getAddress() {
        return this.address;
    }

}
