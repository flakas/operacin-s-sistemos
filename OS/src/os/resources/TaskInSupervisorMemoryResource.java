package os.resources;

import os.core.*;

public class TaskInSupervisorMemoryResource extends Resource {
    private String program;

    public TaskInSupervisorMemoryResource() {
        this.externalName = "TaskInSupervisorMemoryResource";
        this.reusable = false;
    }

    public TaskInSupervisorMemoryResource(String program) {
        this();
        this.program = program;
    }

    public String getProgram() {
        return this.program;
    }

}
