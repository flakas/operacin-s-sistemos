package os.resources;

import os.core.*;
import os.rm.Word;

public class OutputToGeneralMemoryResource extends Resource {
    private Word address;
    private Word value;

    public OutputToGeneralMemoryResource() {
        this.externalName = "OutputToGeneralMemoryResource";
        this.reusable = false;
    }

    public OutputToGeneralMemoryResource(Word address, Word value) {
        this();
        this.address = address;
        this.value = value;
    }

    public Word getAddress() {
        return this.address;
    }

    public Word getValue() {
        return this.value;
    }

}
