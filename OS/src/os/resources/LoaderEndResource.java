package os.resources;

import os.core.*;
import os.rm.Word;

public class LoaderEndResource extends Resource {
    private Word cs;
    private Word ds;
    private Word ss;
    private Word ptr;

    public LoaderEndResource() {
        this.externalName = "LoaderEndResource";
        this.reusable = false;
    }

    public LoaderEndResource(Word cs, Word ds, Word ss, Word ptr) {
        this();
        this.cs = cs;
        this.ds = ds;
        this.ss = ss;
        this.ptr = ptr;
    }

    public Word getCS() {
        return this.cs;
    }

    public Word getDS() {
        return this.ds;
    }

    public Word getSS() {
        return this.ss;
    }

    public Word getPTR() {
        return this.ptr;
    }

    @Override
    public void distribute() {
        this.distributeToReceivers();
    }
}
