package os.resources;

import os.core.*;

public class GeneralWriterEndResource extends Resource {
    public GeneralWriterEndResource() {
        this.externalName = "GeneralWriterEndResource";
        this.reusable = false;
    }

    @Override
    public void distribute() {
        this.distributeToReceivers();
    }
}
