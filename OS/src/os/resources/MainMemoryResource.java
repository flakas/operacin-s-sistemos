package os.resources;

import java.util.ArrayList;

import os.core.*;
import os.rm.Word;
import os.ui.UserInterface;

public class MainMemoryResource extends Resource {
    private ArrayList<MainMemoryResource> memory;
    private Word address;
    private boolean container;

    public MainMemoryResource() {
        this.externalName = "MainMemoryResource";
        this.reusable = true;
    }

    public MainMemoryResource(Word address) {
        this();
        this.address = address;
    }

    public MainMemoryResource(ArrayList<MainMemoryResource> memory) {
        this();
        this.memory = memory;
        this.container = true;
    }

    public boolean isContainer() {
        return this.container;
    }

    public Word getAddress() {
        return this.address;
    }

    public ArrayList<MainMemoryResource> getMemory() {
        return this.memory;
    }

    public void clearElements() {
        this.memory.clear();
    }

    @Override
    public void release(Resource res) {
        UserInterface.log("[MainMemoryResource] Releasing MainMemoryResource: " + res, 3);
        if (!((MainMemoryResource)res).isContainer()) {
            this.resourceElements.add(res);
        } else {
            for (MainMemoryResource item : ((MainMemoryResource)res).getMemory()) {
                this.resourceElements.add(item);
            }
            // Remove any unneeded references
            ((MainMemoryResource) res).clearElements();
        }
    }

    @Override
    public void distribute() {
        for (int i = 0; i < this.waitingProcs.size(); i += 1) {
            WaitingProcess wp = this.waitingProcs.get(0);
            if (wp.getAmount() <= this.resourceElements.size()) {
                UserInterface.log("[MainMemoryResource] Trying to give " + wp.getAmount() + " blocks of memory to " + wp.getProcess(),2);
                ArrayList<MainMemoryResource> elements = new ArrayList<MainMemoryResource>();
                for (int j = 0; j < wp.getAmount(); j += 1) {
                    elements.add((MainMemoryResource) this.resourceElements.get(j));
                }
                kernel.giveResource(wp.getProcess(), new MainMemoryResource(elements));
            } else {
                UserInterface.log("[MainMemoryResource] Not enough blocks (needed: " + wp.getAmount() + ", got: " + this.resourceElements.size() + "), asked by " + wp.getProcess(),2);
            }
        }
    }

    @Override
    public void removeElement(Resource res) {
        for (MainMemoryResource r : ((MainMemoryResource) res).getMemory()) {
            super.removeElement(r);
        }
    }

    @Override
    public String toString() {
        if (this.isContainer()) {
            return super.toString() + "[CONTAINER]";
        } else {
            return super.toString();
        }
    }
}
