package os.resources;

import os.core.*;

public class InputFromExternalDeviceResource extends Resource {
    public InputFromExternalDeviceResource() {
        this.externalName = "InputFromExternalDeviceResource";
        this.reusable = false;
    }

}
