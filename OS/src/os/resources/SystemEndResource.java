package os.resources;

import os.core.Process;
import os.core.*;

public class SystemEndResource extends Resource {
    public SystemEndResource() {
        this.externalName = "SystemEndResource";
        this.reusable = false;
    }

}
