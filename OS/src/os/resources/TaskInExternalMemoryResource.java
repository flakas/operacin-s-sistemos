/* NOTE: dokumente: pakrovimo paketas */

package os.resources;

import os.core.Process;
import os.core.*;
import os.rm.Word;

public class TaskInExternalMemoryResource extends Resource {
    private Word address;

    public TaskInExternalMemoryResource() {
        this.externalName = "TaskInExternalMemoryResource";
        this.reusable = false;
    }

    public TaskInExternalMemoryResource(Word address) {
        this();
        this.address = address;
    }

    public Word getAddress() {
        return this.address;
    }

}
