package os.resources;

import os.core.*;
import os.rm.Word;

public class InputFromExternalMemoryResource extends Resource {
    private Word address;

    public InputFromExternalMemoryResource() {
        this.externalName = "InputFromExternalMemoryResource";
        this.reusable = false;
    }

    public InputFromExternalMemoryResource(Word address) {
        this();
        this.address = address;
    }

    public Word getAddress() {
        return this.address;
    }

}
