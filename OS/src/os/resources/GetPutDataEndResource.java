package os.resources;

import java.util.ArrayList;

import os.core.*;
import os.rm.Word;

public class GetPutDataEndResource extends Resource {
    private ArrayList<Word> input;

    public GetPutDataEndResource() {
        this.externalName = "GetPutDataEndResource";
        this.reusable = false;
    }

    public GetPutDataEndResource(ArrayList<Word> input) {
        this();
        this.input = input;
    }

    public ArrayList<Word> getInput() {
        return this.input;
    }

    @Override
    public void distribute() {
        this.distributeToReceivers();
    }
}
