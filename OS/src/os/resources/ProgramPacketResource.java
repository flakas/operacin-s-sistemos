package os.resources;

import os.core.*;
import os.rm.Word;
import os.resources.*;

public class ProgramPacketResource extends Resource {
    private Word address;
    private MainMemoryResource memory;

    public ProgramPacketResource() {
        this.externalName = "ProgramPacketResource";
        this.reusable = false;
    }

    public ProgramPacketResource(Word address, Resource memory) {
        this();
        this.address = address;
        this.memory = (MainMemoryResource) memory;
    }

    public Word getAddress() {
        return this.address;
    }

    public MainMemoryResource getMemory() {
        return this.memory;
    }

}
