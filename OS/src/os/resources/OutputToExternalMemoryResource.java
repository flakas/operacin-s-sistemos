package os.resources;

import java.util.ArrayList;

import os.core.*;
import os.rm.Word;

public class OutputToExternalMemoryResource extends Resource {
    private ArrayList<Word> words;
    private Word word;
    private Word address;

    public OutputToExternalMemoryResource() {
        this.externalName = "OutputToExternalMemoryResource";
        this.reusable = false;
    }

    public OutputToExternalMemoryResource(Word word, Word address) {
        this();
        this.word = word;
        this.address = address;
    }

    public OutputToExternalMemoryResource(ArrayList<Word> words, Word address) {
        this();
        this.words = words;
        this.address = address;
    }

    public ArrayList<Word> getWords() {
        return this.words;
    }

    public Word getWord() {
        return this.word;
    }

    public Word getAddress() {
        return this.address;
    }

    public boolean outputMany() {
        return this.words != null;
    }

    public boolean outputOne() {
        return this.word != null;
    }

}
