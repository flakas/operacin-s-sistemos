package os.resources;

import os.core.*;
import os.rm.Word;

public class ExternalWriterEndResource extends Resource {
    private Word address;

    public ExternalWriterEndResource() {
        this.externalName = "ExternalWriterEndResource";
        this.reusable = false;
    }

    public ExternalWriterEndResource(Word address) {
        this();
        this.address = address;
    }

    public Word getAddress() {
        return this.address;
    }

    @Override
    public void distribute() {
        this.distributeToReceivers();
    }
}
