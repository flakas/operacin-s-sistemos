package os.resources;

import os.core.*;

public class ExternalMemoryResource extends Resource {
    public ExternalMemoryResource() {
        this.externalName = "ExternalMemoryResource";
        this.reusable = true;
    }
}
