package os.resources;

import os.core.*;

public class InterruptResource extends Resource {
    private String type;
    private int value;

    public InterruptResource() {
        this.externalName = "InterruptResource";
        this.reusable = false;
    }

    public InterruptResource(String type, int value) {
        this();
        this.type = type;
        this.value = value;
    }

    public String getType() {
        return this.type;
    }

    public int getValue() {
        return this.value;
    }

    @Override
    public void distribute() {
        this.distributeToReceivers();
    }
}
