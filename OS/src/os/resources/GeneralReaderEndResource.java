package os.resources;

import os.core.*;
import os.rm.Word;

public class GeneralReaderEndResource extends Resource {
    private Word result;

    public GeneralReaderEndResource() {
        this.externalName = "GeneralReaderEndResource";
        this.reusable = false;
    }

    public GeneralReaderEndResource(Word result) {
        this();
        this.result = result;
    }

    public Word getResult() {
        return this.result;
    }

    @Override
    public void distribute() {
        this.distributeToReceivers();
    }
}
