package os.rm;

import java.util.ArrayList;

public class OutChannel extends Channel {

    public void exchange(ArrayList<Word> output) {
        for (Word w : output) {
            for (int i = 0; i < Memory.WORD_SIZE; i++) {
                short b = w.getByte(i);
                System.out.print((char)b);
            }
        }
        RM.getInstance().setIOI((byte)2);
    }
}
