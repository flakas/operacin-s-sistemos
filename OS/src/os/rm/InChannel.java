package os.rm;

import os.ui.UserInterface;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;

public class InChannel extends Channel {

    public ArrayList<Word> exchange() {
        short[] wordBuf = new short[Memory.WORD_SIZE];
        for (int i = Memory.WORD_SIZE - 1; i >= 0; i -= 1) {
            wordBuf[i] = 0;
        }
        int i = 0;
        byte b;
        Word word;
        ArrayList<Word> words = new ArrayList<Word>();

        DataInputStream stream = new DataInputStream(System.in);
        try
        {
            do
            {
                b = stream.readByte();
                if (b != '\n' && b != 0) {
                    wordBuf[i++] = b;
                }
                if (i == Memory.WORD_SIZE || b == '\n' || b == 0) {
                    i = 0;
                    word = new Word(wordBuf);
                    words.add(word);
                    wordBuf = new short[4];
                }
            } while (b != '\n' && b != 0);
        }
        catch (IOException e)
        {
            UserInterface.log("[RM] Input error in InChannel.",2);
        }
        RM.getInstance().setIOI((byte)1);
        return words;
    }
}
