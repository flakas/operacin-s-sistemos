package os.rm;

public class InterruptException extends Exception {
    private byte code;
    public InterruptException(byte code) {
        super();
        this.code = code;
    }

    public byte getCode() {
        return this.code;
    } 
}
