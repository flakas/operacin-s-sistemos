package os.processes;

import java.util.ArrayList;

import os.core.Process;
import os.resources.*;
import os.rm.*;
import os.ui.UserInterface;

public class Loader extends Process {

    public Loader() {
        this.externalName = "Loader";
    }

    public void execute() {
        UserInterface.log("Executing " + this,2);
        switch (this.IC) {
            case 0:
                //kernel.askForResource(this, "NonExistingResource");
                // TODO: uncomment when ready
                kernel.askForResource(this, "ProgramPacketResource");
                break;
            case 1:
                ProgramPacketResource res = (ProgramPacketResource) this.ownedResources.get(0);
                kernel.freeResource(this, new InputFromExternalMemoryResource(res.getAddress()));
                kernel.askForResource(this, "ExternalReaderEndResource");
                break;
            case 2:
                ExternalReaderEndResource exres = (ExternalReaderEndResource) this.ownedResources.get(1);
                ArrayList<MainMemoryResource> memory = ((ProgramPacketResource) this.ownedResources.get(0)).getMemory().getMemory();
                ArrayList<Word> program = exres.getReadData();
                Memory mainMemory = RM.getInstance().getMemory();

                // Calculate register values
                Word cs = memory.get(0).getAddress().copy();
                Word ds = memory.get(RM.NUMBER_OF_CS_BLOCKS - 1).getAddress().copy();
                Word ss = memory.get(RM.NUMBER_OF_CS_BLOCKS + RM.NUMBER_OF_DS_BLOCKS - 1).getAddress().copy();
                Word ptr = memory.get(RM.NUMBER_OF_CS_BLOCKS + RM.NUMBER_OF_DS_BLOCKS + RM.NUMBER_OF_SS_BLOCKS).getAddress().copy();

                // Load PTR table
                Word address = ptr.copy();
                try {
                    for (int i = 0; i < memory.size(); i++) {
                        mainMemory.setWordReal(address, memory.get(i).getAddress().copy());
                        address = RM.internalAdd(address, Word.ONE);
                    }
                } catch (InterruptException ex) {
                    UserInterface.log("[LOADER] Loader cannot set word, got exception: " + ex,2);
                    // Ignore any interrupts
                }

                this.loadProgramToVM(cs, ds, program);

                kernel.freeResource(this, new LoaderEndResource(cs, ds, ss, ptr), this.ownedResources.get(0).getSender());

                // Clean up
                kernel.freeResource(this, this.ownedResources.get(1));
                kernel.freeResource(this, this.ownedResources.get(0));
                this.resetIC();
                break;
        }
    }

    private void loadProgramToVM(Word cs, Word ds, ArrayList<Word> program) {
        int wordCounter = 0;
        int currentWord = 0;
        Word w = program.get(currentWord);

        Word address = ds.copy();
        Memory mainMemory = RM.getInstance().getMemory();

        try {
            // Move data to DS
            if ((new Word(new short[] {'D', 'A', 'T', 'A'})).equals(w)) {
                Word code = new Word(new short[] {'C', 'O', 'D', 'E'});
                Word db = new Word(new short[] {'D', 'B', ' ', ' '});
                Word dw = new Word(new short[] {'D', 'W', ' ', ' '});
                currentWord += 1;
                w = program.get(currentWord);
                do {
                    if (wordCounter + 1 > RM.NUMBER_OF_DS_BLOCKS * Memory.NUM_OF_WORDS_PER_BLOCK) {
                        throw new RuntimeException("Wrong program file structure, too many data blocks");
                    }
                    if(w.equals(db)) {
                        wordCounter += 1;
                        currentWord += 1;
                        w = program.get(currentWord);
                        mainMemory.setWordReal(address, w);
                        address = RM.internalAdd(address, Word.ONE);
                    } else if (w.equals(dw)) {
                        wordCounter += 1;
                        currentWord += 1;
                        w = program.get(currentWord);
                        currentWord += 1;
                        Word w2 = program.get(currentWord);
                        //for (int i = 0; i < Memory.WORD_SIZE; i += 1) {
                            //if (w.getByte(i) >= 65) {
                                //w.setByte(i, (short)(w.getByte(i) - 65 + 10)); // 65 = A (ASCII)
                            //} else {
                                //w.setByte(i, (short)(w.getByte(i) - 48)); // 48 = 0 (ASCII)
                            //}
                        //}
                        w = VM.paramToAddressTwoWords(w2, w);
                        //w = this.vm.paramToAddress(w);
                        mainMemory.setWordReal(address, w);
                        address = RM.internalAdd(address, Word.ONE);
                    } else if (w.equals(code)) {
                        break;
                    } else {
                        throw new RuntimeException("Wrong program file structure ");
                    }
                    currentWord += 1;
                    w = program.get(currentWord);
                } while (w.equals(code) != true);
            } else {
                throw new RuntimeException("Wrong program file structure. It should start with DATA, started with " + w);
            }
        }
        catch (InterruptException e) {
            UserInterface.log("[Loader] Error moving data to DS",2);
        }

        wordCounter = 0;
        address = cs.copy();
        try {
            // Move code to CS
            if ((new Word(new short[] {'C', 'O', 'D', 'E'})).equals(w)) {
                Word halt = new Word(new short[] {'H', 'A', 'L', 'T'});
                do {
                    wordCounter += 1;
                    if (wordCounter > RM.NUMBER_OF_CS_BLOCKS * Memory.NUM_OF_WORDS_PER_BLOCK) {
                        throw new RuntimeException("Wrong program file structure, too many code blocks");
                    }
                    currentWord += 1;
                    w = program.get(currentWord);
                    mainMemory.setWordReal(address, w);
                    address = RM.internalAdd(address, Word.ONE);
                } while (w.equals(halt) != true);
            } else {
                throw new RuntimeException("Wrong program file structure");
            }
        }
        catch (InterruptException e) {
            UserInterface.log("[Loader] Error moving code to CS",2);
        }
    }
}
