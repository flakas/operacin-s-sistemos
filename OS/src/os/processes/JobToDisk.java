package os.processes;

import os.core.Process;
import os.core.Resource;
import os.resources.*;
import os.rm.Word;
import os.ui.UserInterface;

public class JobToDisk extends Process {

    public JobToDisk() {
        this.externalName = "JobToDisk";
    }

    public void execute() {
        UserInterface.log("Executing " + this,2);
        switch (this.IC) {
            case 0:
                kernel.askForResource(this, "TaskSyntaxCheckedResource");
                break;
            case 1:
                kernel.askForResource(this, "ExternalMemoryResource");
                break;
            case 2:
                TaskSyntaxCheckedResource resource = (TaskSyntaxCheckedResource) this.ownedResources.get(0);
                kernel.freeResource(this, new OutputToExternalMemoryResource(resource.getProgram(), new Word(new short[] {0, 0, 0, 0})));
                kernel.askForResource(this, "ExternalWriterEndResource");
                break;
            case 3:
                ExternalWriterEndResource address = (ExternalWriterEndResource) this.ownedResources.get(2);
                kernel.freeResource(this, new TaskInExternalMemoryResource(address.getAddress()));

                //Cleanup
                kernel.freeResource(this, this.ownedResources.get(2));
                kernel.freeResource(this, this.ownedResources.get(1));
                kernel.freeResource(this, this.ownedResources.get(0));
                this.resetIC();
                break;
        }

    }
}
