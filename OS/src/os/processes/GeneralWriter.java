package os.processes;

import os.core.Process;
import os.resources.*;
import os.rm.*;
import os.ui.UserInterface;

public class GeneralWriter extends Process {

    public GeneralWriter() {
        this.externalName = "GeneralWriter";
    }

    public void execute() {
        UserInterface.log("Executing " + this,2);
        switch (this.getIC()) {
            case 0:
                kernel.askForResource(this, "OutputToGeneralMemoryResource");
                break;
            case 1:
                OutputToGeneralMemoryResource res = (OutputToGeneralMemoryResource) this.ownedResources.get(0);
                try {
                    rm.getMemory().setWord(RM.internalAdd(rm.getGS(), res.getAddress()), res.getValue());
                } catch (InterruptException ex) {
                    UserInterface.log("[GeneralWriter] InterruptException - " + ex,1);
                }
                kernel.freeResource(this, new GeneralWriterEndResource(), res.getSender());

                // Cleanup
                kernel.freeResource(this, res);
                this.resetIC();
                break;
        }
    }
}
