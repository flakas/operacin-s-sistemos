package os.processes;

import os.core.Process;
import os.resources.*;
import os.rm.*;
import os.ui.UserInterface;

public class GeneralReader extends Process {

    public GeneralReader() {
        this.externalName = "GeneralReader";
    }

    public void execute() {
        UserInterface.log("Executing " + this,2);
        switch (this.getIC()) {
            case 0:
                kernel.askForResource(this, "InputFromGeneralMemoryResource");
                break;
            case 1:
                InputFromGeneralMemoryResource res = (InputFromGeneralMemoryResource) this.ownedResources.get(0);
                Word result = null;

                try {
                    result = rm.getMemory().getWord(RM.internalAdd(rm.getGS(), res.getAddress()));
                } catch (InterruptException ex) {
                    UserInterface.log("[GeneralWriter] InterruptException - " + ex,1);
                }
                kernel.freeResource(this, new GeneralReaderEndResource(result), res.getSender());

                // Cleanup
                kernel.freeResource(this, res);
                this.resetIC();
                break;
        }
    }
}
