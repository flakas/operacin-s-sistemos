package os.processes;

import os.core.Process;
import os.core.Resource;
import os.resources.*;
import os.rm.RM;
import os.ui.UserInterface;

public class Interrupt extends Process {

    public Interrupt() {
        this.externalName = "Interrupt";
    }

    public void execute() {
        UserInterface.log("Executing " + this,2);
        switch (this.IC) {
            case 0:
                kernel.askForResource(this, "InterruptEventResource");
                break;
            case 1:
                Resource res = this.ownedResources.get(0);
                RM rm = RM.getInstance();
                String type = "NoInterruptType";
                int value = -1;
                if (rm.getPI() != 0) {
                    type = "PI";
                    value = rm.getPI();
                } else if (rm.getSI() != 0) {
                    type = "SI";
                    value = rm.getSI();
                } else if (rm.getTI() <= 0) {
                    type = "TI";
                    value = rm.getTI();
                    rm.resetTI();
                } else if (rm.getIOI() != 0) {
                    type = "IOI";
                    value = rm.getIOI();
                } else {
                    UserInterface.log("[Interrupt] Cannot determine interrupt type",2);
                }
                kernel.freeResource(this, new InterruptResource(type, value), res.getSender().getParent());

                // Cleanup
                kernel.freeResource(this, res);
                this.resetIC();
                break;
        }

    }
}
