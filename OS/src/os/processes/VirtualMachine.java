package os.processes;

import java.util.ArrayList;

import os.rm.*;
import os.resources.*;
import os.core.Process;
import os.ui.UserInterface;
import java.io.IOException;

public class VirtualMachine extends Process {
    private Word CS;
    private Word DS;
    private Word SS;
    private Word SP;
    private Word PTR;
    private Word IP;
    private Word SF;
    private Word currentCommand;
    private byte[] semaphores;
    private Object[] currentParams;

    public VirtualMachine() {
        this.externalName = "VirtualMachine";
        this.priority = 0;
    }

    public VirtualMachine (Word cs, Word ds, Word ss, Word sp, Word ptr) {
        this();
        this.CS = cs;
        this.DS = ds;
        this.SS = ss;
        this.SP = sp;
        this.PTR = ptr;
        this.IP = new Word(new short[] {0, 0, 0, 0});
        this.SF = new Word(new short[] {0, 0, 0, 0});
        this.semaphores = new byte[2];
        this.currentParams = new Object[2];
    }


    public void execute() {
        UserInterface.log("Executing " + this,2);
        RM.getInstance().setMode((byte)1);
        this.step();
        RM.getInstance().setMode((byte)0);
        if (RM.getInstance().test()) {
            //InterruptEventResource ier = new InterruptEventResource();
            //ier.setSender(this);
            kernel.freeResource(this, new InterruptEventResource());
        }
    }

    public void finalize() {
        // Forcefully release any semaphores
        for (int i = 1; i <= 2; i += 1) {
            if (this.semaphores[i - 1] == 1) {
                this.rel(new Word(new short[] {0, 0, 0, (short)i}));
            }
        }
    }

    public Word add(Word op1, Word op2) {
        this.unsetCF();
        this.unsetOF();
        Word result = new Word();
        short carry = 0;
        for (int i = Memory.WORD_SIZE - 1; i >= 0; i -= 1) {
            short sum = 0;
            if (carry == 1) {
                sum += carry;
                carry -= 1;
            }
            sum += op1.getByte(i) + op2.getByte(i);
            if (sum >= RM.NUMERAL_SYSTEM) {
                sum -= RM.NUMERAL_SYSTEM;
                carry += 1;
                if (i > 0) {
                    this.setCF();
                } else {
                    this.setOF();
                }
            }
            result.setByte(i, sum);
        }
        return result;
    }

    public Word sub(Word op1, Word op2) {
        this.unsetCF();
        this.unsetOF();
        Word result = new Word();
        short borrow = 0;
        for (int i = Memory.WORD_SIZE - 1; i >= 0; i -= 1) {
            short difference = 0;
            if (borrow == 1) {
                difference -= 1;
                borrow -= 1;
            }
            difference += op1.getByte(i) - op2.getByte(i);
            if (difference < 0) {
                difference += RM.NUMERAL_SYSTEM;
                borrow += 1;
                if (i > 0) {
                    this.setCF();
                } else {
                    this.setOF();
                }
            }

            result.setByte(i, difference);
        }
        return result;
    }

    public Word mul(Word op1, Word op2) {
        this.unsetCF();
        this.unsetOF();
        Word result = new Word();
        int carry = 0;
        for (int i = Memory.WORD_SIZE - 1; i >= 0; i -= 1) {
            for (int j = Memory.WORD_SIZE - 1; j >= 0; j -= 1) {
                int pos = i + j - (Memory.WORD_SIZE - 1);
                if (pos >= Memory.WORD_SIZE || pos < 0) { continue; }
                int multiplication = 0;
                if (carry > 0) {
                    multiplication += carry;
                    carry = 0;
                }
                multiplication += (op1.getByte(i) * op2.getByte(j));

                if (multiplication >= RM.NUMERAL_SYSTEM) {
                    carry += (multiplication - (multiplication % RM.NUMERAL_SYSTEM)) / RM.NUMERAL_SYSTEM;
                    multiplication = multiplication % RM.NUMERAL_SYSTEM;
                    if (i > 0) {
                        this.setCF();
                    } else {
                        this.setOF();
                    }
                }

                multiplication = result.getByte(pos) + multiplication;

                result.setByte(pos, (short)multiplication);
            }
        }
        return result;
    }

    public Word div(Word op1, Word op2)
    throws InterruptException {
        Word result;
        if (op2.equals(Word.ZERO)) {
            throw new InterruptException((byte)2);
        }
        long opInt1 = op1.toLong();
        long opInt2 = op2.toLong();
        result = Word.parseLong(opInt1 / opInt2);

        return result;
    }

    public Word mod(Word op1, Word op2)
    throws InterruptException {
        Word result;
        if (op2.equals(Word.ZERO)) {
            throw new InterruptException((byte)2);
        }

        result = this.sub(op1, this.mul(op2, this.div(op1, op2)));

        return result;
    }

    public Word dec(Word op) {
        return this.sub(op, Word.ONE);
    }

    public Word inc(Word op) {
        return this.add(op, Word.ONE);
    }

    public Word xor(Word op1, Word op2) {
        Word result = new Word();
        for (int i = 0; i < Memory.WORD_SIZE; i++) {
            result.setByte(i, (short)(op1.getByte(i) ^ op2.getByte(i)));
        }
        return result;
    }

    public Word and(Word op1, Word op2) {
        Word result = new Word();
        for (int i = 0; i < Memory.WORD_SIZE; i++) {
            result.setByte(i, (short)(op1.getByte(i) & op2.getByte(i)));
        }
        return result;
    }

    public Word or(Word op1, Word op2) {
        Word result = new Word();
        for (int i = 0; i < Memory.WORD_SIZE; i++) {
            result.setByte(i, (short)(op1.getByte(i) | op2.getByte(i)));
        }
        return result;
    }

    public Word not(Word op) {
        Word result = this.xor(op, new Word(new short[] {255, 255, 255, 255}));
        return result;
    }

    public void cmp(Word op1, Word op2) {
        this.unsetZF();
        if (op1.equals(op2)) {
            this.setZF();
            this.setCmpF((short)0);
        } else if (op1.compareTo(op2) == -1) {
            this.unsetZF();
            this.setCmpF((short)1);
        } else if (op1.compareTo(op2) == 1) {
            this.unsetZF();
            this.setCmpF((short)2);
        }
    }

    public boolean jump(Word op) 
    throws InterruptException {
        int numberOfWords = 0;
        for (int i = Memory.WORD_SIZE - 1; i >= 0; i -= 1) {
            numberOfWords = (int)(numberOfWords + (op.getByte(i) * Math.pow(RM.NUMERAL_SYSTEM, Memory.WORD_SIZE - 1 - i)));
        }

        if (numberOfWords >= RM.BLOCKS_PER_VM * RM.NUMBER_OF_CS_BLOCKS * Memory.NUM_OF_WORDS_PER_BLOCK) {
            throw new InterruptException((byte)3);
        }

        for (int i = 0; i < Memory.WORD_SIZE; i++) {
            this.IP.setByte(i, op.getByte(i));
        }
        return true;
    }

    public boolean je(Word op)
    throws InterruptException {
        if (this.getSF().getByte(1) == (short)0) {
            return jump(op);
        }
        return false;
    }

    public boolean jl(Word op)
    throws InterruptException {
        if (this.getSF().getByte(1) == (short)1) {
            return jump(op);
        }
        return false;
    }

    public boolean jz(Word op)
    throws InterruptException {
        if (this.getSF().getByte(3) == (short)1) {
            return jump(op);
        }
        return false;
    }

    public boolean jnz(Word op)
    throws InterruptException {
        if (this.getSF().getByte(3) == (short)1) {
            return jump(op);
        }
        return false;
    }

    public boolean jg(Word op)
    throws InterruptException {
        if (this.SF.getByte(1) == (short)2) {
            return this.jump(op);
        }
        return false;
    }

    public boolean jc(Word op)
    throws InterruptException {
        if (this.SF.getByte(2) == (short)1) {
            return this.jump(op);
        }
        return false;
    }

    public boolean cd(Word displ, Word value)
    throws InterruptException {
        RM rm = RM.getInstance();

        return this.setVMWord((short)1, displ, value);
    }

    public Word gd(Word displ)
    throws InterruptException {
        RM rm = RM.getInstance();

        return this.getVMWord((short)1, displ);
    }

    public boolean acq(Word x) {
        RM rm = RM.getInstance();
        boolean status = rm.acquireSemaphore(x.getByte(3));
        if (status) {
            this.gotSemaphore(x.getByte(3));
        }
        return status;
    }

    public void rel(Word x) {
        RM rm = RM.getInstance();
        rm.releaseSemaphore(x.getByte(3));
        this.releasedSemaphore(x.getByte(3));
    }

    public void ie(Word address)
    throws InterruptException {
        if (RM.getInstance().getMode() != (byte)1) {
            RM.getInstance().setSI((byte)3);
            this.setCurrentParam(1, address);
            return;
        }
        throw new InterruptException((byte)5);
    }

    public void oe(Word address, Word value)
    throws InterruptException {
        if (RM.getInstance().getMode() != (byte)1) {
            RM.getInstance().setSI((byte)4);
            this.setCurrentParam(1, address);
            this.setCurrentParam(2, value);
            return;
        }
        throw new InterruptException((byte)5);
    }

    public void ig(Word address) 
    throws InterruptException {
        if (RM.getInstance().getMode() != (byte)1) {
            RM.getInstance().setSI((byte)6);
            this.setCurrentParam(1, address);
            return;
        }
        throw new InterruptException((byte)5);
    }

    public void og(Word address, Word value) 
    throws InterruptException {
        if (RM.getInstance().getMode() != (byte)1) {
            RM.getInstance().setSI((byte)7);
            this.setCurrentParam(1, address);
            this.setCurrentParam(2, value);
            return;
        }
        throw new InterruptException((byte)5);
    }

    public void in(Word address) {
        if (RM.getInstance().getMode() != (byte)1) {
            RM.getInstance().setSI((byte)1);
            this.setCurrentParam(1, address);
        }
    }

    public void out(Word address) {
        //if (RM.getInstance().getMode() != (byte)1) {
            RM.getInstance().setSI((byte)2);
            ArrayList<Word> output = new ArrayList<Word>();
            boolean hasNullBytes = false;
            try {
                while (!hasNullBytes) {
                    Word w = this.getVMWord((short)1, address);
                    output.add(w);
                    for (int i = 0; i < Word.WORD_SIZE; i += 1) {
                        if (w.getByte(i) == 0) {
                            hasNullBytes = true;
                            break;
                        }
                    }
                    address = RM.internalAdd(address, Word.ONE);
                }
            } catch (InterruptException ex) {
                UserInterface.log("[VirtualMachine] Interrupt exception in OUT command: " + ex,2);
            }
            UserInterface.log("OUT vidus: " + output,3);
            this.setCurrentParam(1, output);
        //}
    }

    public void roll() {
        Runtime rt = Runtime.getRuntime();
        try {
            java.lang.Process proc = rt.exec("google-chrome https://www.youtube.com/watch?v=dQw4w9WgXcQ");
        } catch (IOException ex) {

        }
    }

    public void notRecognized(Word w)
    throws InterruptException {
        this.setCurrentParam(1, w);
        throw new InterruptException((byte)1);
    }

    public void halt() {
        RM.getInstance().setSI((byte)5);
    }

    // Internal usage only
    // Segment: 0-CS
    //          1-Ds
    //          2-SS
    // address - internal VM address
    public boolean setVMWord(short segment, Word address, Word value)
    throws InterruptException {
        Memory mem = RM.getInstance().getMemory();
        Word segAddress;
        switch (segment) {
            case 0: segAddress = this.CS;
                    break;
            case 1: segAddress = this.DS;
                    break;
            case 2: segAddress = this.SS;
                    break;
            default: return false;
        }
        RM rm = RM.getInstance();

        return this.setWord(RM.internalAdd(segAddress, address), value);
    }

    public boolean setWord(Word address, Word value)
    throws InterruptException {
        Word blockAddress = this.getRealBlockAddress(address);
        Word w = new Word(new short[]{0,0,0,Word.getRightNibble(address.getByte(3))});
        Memory mem = RM.getInstance().getMemory();
        UserInterface.log("[VM] setting word address: " + RM.internalAdd(blockAddress, w) + " value:" + value,2);
        return mem.setWordReal(RM.internalAdd(blockAddress, w), value);
    }


    // Internal usage only
    // Segment: 0-CS
    //          1-Ds
    //          2-SS
    // address - internal VM address
    public Word getVMWord(short segment, Word address)
    throws InterruptException {
        Word segAddress;
        switch (segment) {
            case 0: segAddress = this.CS;
                    break;
            case 1: segAddress = this.DS;
                    break;
            case 2: segAddress = this.SS;
                    break;
            default: return null;
        }
        RM rm = RM.getInstance();

        return this.getWord(RM.internalAdd(segAddress, address));
    }

    public Word getWord(Word address)
    throws InterruptException {
        Word blockAddress = this.getRealBlockAddress(address);
        Word w = new Word(new short[]{0,0,0,Word.getRightNibble(address.getByte(3))});

        Memory mem = RM.getInstance().getMemory();
        return mem.getWordReal(RM.internalAdd(blockAddress, w));
    }

    public Word getRealBlockAddress(Word vmAddress)
    throws InterruptException {
        Memory mem = RM.getInstance().getMemory();
        int ptrIndex = mem.blockIndexFromAddress(this.PTR);
        short block = Word.getLeftNibble(vmAddress.getByte(3));
        return mem.getWordReal(ptrIndex * RM.HEX_SYSTEM + block);
    }

    public void step() {
        RM.getInstance().resetInterrupts();
        this.resetCurrentParams();
        try {
            Word command = this.getVMWord((short)0, this.getIP());
            UserInterface.log("[VM]New Command: " + command.toStringSymbols(),2);
            this.currentCommand = command;
            //Commands
            Word add = new Word(new short[] {'A', 'D', 'D', ' '}),
                 sub = new Word(new short[] {'S', 'U', 'B', ' '}),
                 mul = new Word(new short[] {'M', 'U', 'L', ' '}),
                 div = new Word(new short[] {'D', 'I', 'V', ' '}),
                 mod = new Word(new short[] {'M', 'O', 'D', ' '}),
                 inc = new Word(new short[] {'I', 'N', 'C', ' '}),
                 dec = new Word(new short[] {'D', 'E', 'C', ' '}),
                 // logic commands
                 xor = new Word(new short[] {'X', 'O', 'R', ' '}),
                 and = new Word(new short[] {'A', 'N', 'D', ' '}),
                 or = new Word(new short[] {'O', 'R', ' ', ' '}),
                 not = new Word(new short[] {'N', 'O', 'T', ' '}),
                 cmp = new Word(new short[] {'C', 'M', 'P', ' '}),
                 // Jump commands
                 jump = new Word(new short[] {'J', 'U', 'M', 'P'}),
                 jg = new Word(new short[] {'J', 'G', ' ', ' '}),
                 je = new Word(new short[] {'J', 'E', ' ', ' '}),
                 jl = new Word(new short[] {'J', 'L', ' ', ' '}),
                 jc = new Word(new short[] {'J', 'C', ' ', ' '}),
                 jz = new Word(new short[] {'J', 'Z', ' ', ' '}),
                 jnz = new Word(new short[] {'J', 'N', 'Z', ' '}),
                 // Data manipulation commands
                 cd = new Word(new short[] {'C', 'D', ' ', ' '}),
                 md = new Word(new short[] {'M', 'D', ' ', ' '}),
                 gd = new Word(new short[] {'G', 'D', ' ', ' '}),
                 acq = new Word(new short[] {'A', 'C', 'Q', ' '}),
                 rel = new Word(new short[] {'R', 'E', 'L', ' '}),
                 ie = new Word(new short[] {'I', 'E', ' ', ' '}),
                 oe = new Word(new short[] {'O', 'E', ' ', ' '}),
                 ig = new Word(new short[] {'I', 'G', ' ', ' '}),
                 og = new Word(new short[] {'O', 'G', ' ', ' '}),
                 // I/O commands
                 ou = new Word(new short[] {'O', 'U', ' ', ' '}),
                 in = new Word(new short[] {'I', 'N', ' ', ' '}),
                 // VM termination command
                 halt = new Word(new short[] {'H', 'A', 'L', 'T'}),
                 roll = new Word(new short[] {'R', 'O', 'L', 'L'});
            Word w1, w2;

            if (command.equals(add)) {
                w2 = this.popStack();
                w1 = this.popStack();
                RM.getInstance().decrementTI((byte)1); // 1 cycle
                this.pushStack(this.add(w1, w2));
            } else if (command.equals(sub)) {
                w2 = this.popStack();
                w1 = this.popStack();
                RM.getInstance().decrementTI((byte)1); // 1 cycle
                this.pushStack(this.sub(w1, w2));
            } else if (command.equals(mul)) {
                w2 = this.popStack();
                w1 = this.popStack();
                RM.getInstance().decrementTI((byte)1); // 1 cycle
                this.pushStack(this.mul(w1, w2));
            } else if (command.equals(div)) {
                w2 = this.popStack();
                w1 = this.popStack();
                RM.getInstance().decrementTI((byte)1); // 1 cycle
                this.pushStack(this.div(w1, w2));
            } else if (command.equals(mod)) {
                w2 = this.popStack();
                w1 = this.popStack();
                RM.getInstance().decrementTI((byte)1); // 1 cycle
                this.pushStack(this.mod(w1, w2));
            } else if (command.equals(inc)) {
                w1 = this.popStack();
                RM.getInstance().decrementTI((byte)1); // 1 cycle
                this.pushStack(this.inc(w1));
            } else if (command.equals(dec)) {
                w1 = this.popStack();
                RM.getInstance().decrementTI((byte)1); // 1 cycle
                this.pushStack(this.dec(w1));
            } else if (command.equals(xor)) {
                w2 = this.popStack();
                w1 = this.popStack();
                RM.getInstance().decrementTI((byte)1); // 1 cycle
                this.pushStack(this.xor(w1, w2));
            } else if (command.equals(and)) {
                w2 = this.popStack();
                w1 = this.popStack();
                RM.getInstance().decrementTI((byte)1); // 1 cycle
                this.pushStack(this.and(w1, w2));
            } else if (command.equals(or)) {
                w2 = this.popStack();
                w1 = this.popStack();
                RM.getInstance().decrementTI((byte)1); // 1 cycle
                this.pushStack(this.or(w1, w2));
            } else if (command.equals(not)) {
                w1 = this.popStack();
                RM.getInstance().decrementTI((byte)1); // 1 cycle
                this.pushStack(this.not(w1));
            } else if (command.equals(cmp)) {
                w2 = this.popStack();
                w1 = this.popStack();
                RM.getInstance().decrementTI((byte)1); // 1 cycle
                this.cmp(w1, w2);
            } else if (command.equals(jump)) {
                w1 = this.popStack();
                RM.getInstance().decrementTI((byte)1); // 1 cycle
                this.jump(w1);
            } else if (command.equals(jg)) {
                w1 = this.popStack();
                RM.getInstance().decrementTI((byte)1); // 1 cycle
                this.jg(w1);
            } else if (command.equals(je)) {
                w1 = this.popStack();
                RM.getInstance().decrementTI((byte)1); // 1 cycle
                this.je(w1);
            } else if (command.equals(jl)) {
                w1 = this.popStack();
                RM.getInstance().decrementTI((byte)1); // 1 cycle
                this.jl(w1);
            } else if (command.equals(jc)) {
                w1 = this.popStack();
                RM.getInstance().decrementTI((byte)1); // 1 cycle
                this.jc(w1);
            } else if (command.equals(jz)) {
                w1 = this.popStack();
                RM.getInstance().decrementTI((byte)1); // 1 cycle
                this.jz(w1);
            } else if (command.equals(jnz)) {
                w1 = this.popStack();
                RM.getInstance().decrementTI((byte)1); // 1 cycle
                this.jnz(w1);
            } else if (command.equals(halt)) {
                RM.getInstance().decrementTI((byte)1); // 1 cycle
                this.halt();
            } else if (command.equals(roll)) {
                this.roll();
            } else {
                // Try to recognize 3 and 2 symbol opcodes
                Word shortCommand = command.copy();
                shortCommand.setByte(3, (short)' ');
                if (shortCommand.equals(acq)) { // Acquire a semaphore
                    w1 = new Word();
                    w1.setByte(3, command.getByte(3));
                    RM.getInstance().decrementTI((byte)1); // 1 cycle
                    this.acq(paramToAddress(w1));
                } else if (shortCommand.equals(rel)) { // Release a semaphore
                    w1 = new Word();
                    w1.setByte(3, command.getByte(3));
                    RM.getInstance().decrementTI((byte)1); // 1 cycle
                    this.rel(paramToAddress(w1));
                } else {
                    shortCommand.setByte(2, (short)' ');
                    if (shortCommand.equals(cd)) { // Copy from stack to DS
                        w1 = new Word();
                        w1.setByte(3, command.getByte(3));
                        w1.setByte(2, command.getByte(2));
                        w2 = this.peekStack();
                        RM.getInstance().decrementTI((byte)1); // 1 cycle
                        this.cd(paramToAddress(w1), w2);
                    } else if (shortCommand.equals(md)) { // Move from stack to DS
                        w1 = new Word();
                        w1.setByte(3, command.getByte(3));
                        w1.setByte(2, command.getByte(2));
                        w2 = this.popStack();
                        RM.getInstance().decrementTI((byte)1); // 1 cycle
                        this.cd(paramToAddress(w1), w2);
                    } else if (shortCommand.equals(gd)) { // Copy from DS to stack
                        w1 = new Word();
                        w1.setByte(3, command.getByte(3));
                        w1.setByte(2, command.getByte(2));
                        RM.getInstance().decrementTI((byte)1); // 1 cycle
                        this.pushStack(this.gd(paramToAddress(w1)));
                    } else if (shortCommand.equals(ie)) { // Copy from external memory to stack
                        Word one = Word.ONE;
                        this.IP = RM.internalAdd(this.getIP(), one);
                        w1 = this.getVMWord((short)0, this.IP); // Get the first op
                        RM.getInstance().decrementTI((byte)3); // 3 cycle
                        this.ie(paramToAddress(w1));
                    } else if (shortCommand.equals(oe)) { // Copy from stack to external memory
                        Word one = Word.ONE;
                        this.IP = RM.internalAdd(this.getIP(), one);
                        w1 = this.getVMWord((short)0, this.IP); // Get the first op
                        RM.getInstance().decrementTI((byte)3); // 3 cycle
                        this.oe(paramToAddress(w1), this.popStack());
                    } else if (shortCommand.equals(ig)) { // Copy from general memory to stack
                        w1 = new Word();
                        w1.setByte(3, command.getByte(3));
                        w1.setByte(2, command.getByte(2));
                        RM.getInstance().decrementTI((byte)3); // 3 cycle
                        this.ig(paramToAddress(w1));
                    } else if (shortCommand.equals(og)) { // Copy from stack to general memory
                        w1 = new Word();
                        w1.setByte(3, command.getByte(3));
                        w1.setByte(2, command.getByte(2));
                        RM.getInstance().decrementTI((byte)3); // 3 cycle
                        this.og(paramToAddress(w1), this.popStack());
                    } else if (shortCommand.equals(ou)) { // Output from DS
                        w1 = new Word();
                        w1.setByte(3, command.getByte(3));
                        w1.setByte(2, command.getByte(2));
                        RM.getInstance().decrementTI((byte)3); // 3 cycle
                        UserInterface.log("[VM] Didelis if komanda OU: " + paramToAddress(w1),3);
                        this.out(paramToAddress(w1));
                    } else if (shortCommand.equals(in)) { // Input to DS
                        w1 = new Word();
                        w1.setByte(3, command.getByte(3));
                        w1.setByte(2, command.getByte(2));
                        RM.getInstance().decrementTI((byte)3); // 3 cycle
                        this.in(paramToAddress(w1));
                    } else { // command not recognized
                        RM.getInstance().decrementTI((byte)1); // 1 cycle
                        this.notRecognized(command);
                    }
                }
            }
        }
        catch (InterruptException e) {
            UserInterface.log("[VM] Got exception: " + e, 3);
            RM.getInstance().setPI(e.getCode());
        }

        this.IP = RM.internalAdd(this.getIP(), Word.ONE);
    }

    public Word popStack()
    throws InterruptException {
        this.SP = RM.internalAdd(this.SP, Word.ONE);
        Word w = this.getVMWord((short)2, this.SP);
        return w;
    }

    /**
     * Returns a copy of SS[SP + 1] Word without modifying stack
     * @return Word SS[SP] word
     */
    public Word peekStack()
    throws InterruptException {
        Word address = RM.internalAdd(this.SP, Word.ONE);
        Word w = this.getVMWord((short)2, address);
        return w;
    }

    /**
     * Returns a copy of SS[SP + 1 + displ] Word without modifying stack
     * @param Word n-th word to peek
     * @return Word SS[SP + displ] word
     */
    private Word peekStack(Word displ)
    throws InterruptException {
        Word address = RM.internalAdd(RM.internalAdd(this.SP, Word.ONE), displ);
        Word w = this.getVMWord((short)2, address);
        return w;
    }

    public void pushStack(Word value)
    throws InterruptException {
        this.setVMWord((short)2, this.SP, value);
        this.SP = RM.internalSub(this.SP, Word.ONE);
    }


    private void setCF() {
        this.SF.setByte(2, (short)1);
    }

    private void unsetCF() {
        this.SF.setByte(2, (short)0);
    }

    private void setOF() {
        this.SF.setByte(0, (short)1);
    }

    private void unsetOF() {
        this.SF.setByte(0, (short)0);
    }

    private void setZF() {
        this.SF.setByte(3, (short)1);
    }

    private void unsetZF() {
        this.SF.setByte(3, (short)0);
    }

    private void setCmpF(short value) {
        this.SF.setByte(1, value);
    }


    // Register setters and getters

    public Word getIP() {
        return this.IP;
    }

    public void setIP(Word IP) {
        this.IP = IP;
    }

    public Word getSF() {
        return this.SF;
    }

    public void setSF(Word SF) {
        this.SF = SF;
    }

    public Word getCS() {
        return this.CS;
    }

    public void setCS(Word CS) {
        this.CS = CS;
    }

    public Word getDS() {
        return this.DS;
    }

    public void setDS(Word DS) {
        this.DS = DS;
    }

    public Word getSS() {
        return this.SS;
    }

    public void setSS(Word SS) {
        this.SS = SS;
    }

    public Word getSP() {
        return this.SP;
    }

    public void setSP(Word SP) {
        this.SP = SP;
    }

    public Word getCurrentCommand() {
        return this.currentCommand;
    }

    // For testing and debugging only
    public void setCurrentCommand(Word c) {
        this.currentCommand = c;
    }

    private void gotSemaphore(short x) {
        this.semaphores[x - 1] = 1;
    }

    private void releasedSemaphore(short x) {
        this.semaphores[x - 1] = 0;
    }

    private boolean hasSemaphore(short x) {
        return this.semaphores[x - 1] == 1;
    }

    private void resetCurrentParams() {
        UserInterface.log("[VM] Resetting current params",2);
        this.currentParams[0] = null;
        this.currentParams[1] = null;
    }

    public Object getCurrentParam(int x) {
        for(Object o : this.currentParams) {
            UserInterface.log("[VM] Current params: " + o,3);
        }
        return this.currentParams[x - 1];
    }

    public void setCurrentParam(int x, Object value) {
        UserInterface.log("[VM] SetCurrentParam vidus: " + value,3);
        UserInterface.log("[VM] SetCurrentParam VM: " + this,3);
        this.currentParams[x - 1] = value;
    }

    public static Word paramToAddress(Word word)
    throws InterruptException {
        Word result = new Word();
        Word w = word.copy();
        final int ASCII_A = 65,
                  ASCII_0 = 48,
                  ASCII_9 = 57,
                  ASCII_LETTER_COMPENSATE = 10;

        UserInterface.log("[VM] paramToAddress got word: " + w, 3);
        for (int i = Memory.WORD_SIZE - 1; i >= 0; i -= 1) {
            UserInterface.log("[VM] paramToAddress byte " + i + " : " + w.getByte(i), 3);
            if (w.getByte(i) >= ASCII_A) {
                w.setByte(i, (short)(w.getByte(i) - ASCII_A + ASCII_LETTER_COMPENSATE));
            }
            else if (w.getByte(i) >= ASCII_0 && w.getByte(i) <= ASCII_9) {
                w.setByte(i, (short)(w.getByte(i) - ASCII_0));
            } else if (w.getByte(i) == 0) {
                // Do nothing if null - it's already a valid address
            } else { // non hex address, throw an interrupt
                throw new InterruptException((byte)3);
            }
        }

        int b = Memory.WORD_SIZE - 1;
        for (int i = Memory.WORD_SIZE - 1; i >= 0; i -= 2) {
            result.setByte(b, (short)(w.getByte(i - 1) * RM.HEX_SYSTEM + w.getByte(i)));
            b -= 1;
        }
        return result;
    }

    public static Word paramToAddressTwoWords(Word low, Word high)
    throws InterruptException {
        low = paramToAddress(low);
        high = paramToAddress(high);
        Word result = new Word();
        result.setByte(0, high.getByte(2));
        result.setByte(1, high.getByte(3));
        result.setByte(2, low.getByte(2));
        result.setByte(3, low.getByte(3));
        return result;
    }

    public String toMemoryString() {
        String ret = "";
        Word address = new Word(new short[] {0, 0, 0, 0});
        for (int i = 0; i < RM.BLOCKS_PER_VM * Memory.NUM_OF_WORDS_PER_BLOCK; i += 1) {
            if (i % 4 == 0) {
                ret += String.format("%8x: ", i);
            }
            try {
                ret += this.getVMWord((short)0, address) + "   ";
            } catch (InterruptException ex) {
                UserInterface.log("[VM] Cannot access VM memory in toMemoryString()",2);
            }
            if ((i+1) % 4 == 0) {
                ret += "\n";
            }
            if ((i+1) % 16 == 0) {
                ret += "\n";
            }
            address = RM.internalAdd(address, Word.ONE);
        }
        ret += "\n";
        return ret;
    }
}
