package os.processes;

import os.core.Process;
import os.resources.*;
import os.ui.UserInterface;

public class ExternalWriter extends Process {

    public ExternalWriter() {
        this.externalName = "ExternalWriter";
    }

    public void execute() {
        UserInterface.log("Executing " + this,2);
        switch (this.getIC()) {
            case 0:
                kernel.askForResource(this, "OutputToExternalMemoryResource");
                break;
            case 1:
                OutputToExternalMemoryResource res = (OutputToExternalMemoryResource) this.ownedResources.get(0);
                if (res.outputMany()) {
                    rm.getExternalMemory().setWords(res.getAddress(), res.getWords());
                } else {
                    rm.getExternalMemory().setWord(res.getAddress(), res.getWord());
                }
                kernel.freeResource(this, new ExternalWriterEndResource(res.getAddress()), res.getSender());

                // Cleanup
                kernel.freeResource(this, res);
                this.resetIC();
                break;
        }
    }
}
