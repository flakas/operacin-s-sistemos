package os.processes;


import os.core.Kernel;
import os.core.Process;
import os.resources.*;
import os.rm.*;
import os.ui.UserInterface;

public class StartStop extends Process {

    public StartStop() {
        this.externalName = "StartStop";
    }

    public void execute() {
        UserInterface.log("[StartStop] Executing " + this,2);
        Kernel kernel = Kernel.getInstance();
        switch (this.IC) {
            case 0:
                // Create system processes
                kernel.createProcess(this, new MainProc());
                kernel.createProcess(this, new Loader());
                kernel.createProcess(this, new JSC());
                kernel.createProcess(this, new JobToDisk());
                kernel.createProcess(this, new Interrupt());
                kernel.createProcess(this, new GetPutData());
                kernel.createProcess(this, new GeneralWriter());
                kernel.createProcess(this, new GeneralReader());
                kernel.createProcess(this, new ExternalWriter());
                kernel.createProcess(this, new ExternalReader());

                // Create resources
                kernel.createResource(this, new ExternalMemoryResource());
                kernel.createResource(this, new ExternalReaderEndResource());
                kernel.createResource(this, new ExternalWriterEndResource());
                kernel.createResource(this, new GeneralMemoryResource());
                kernel.createResource(this, new GeneralReaderEndResource());
                kernel.createResource(this, new GeneralWriterEndResource());
                kernel.createResource(this, new GetPutDataEndResource());
                kernel.createResource(this, new InputFromExternalDeviceResource());
                kernel.createResource(this, new InputFromExternalMemoryResource());
                kernel.createResource(this, new InputFromGeneralMemoryResource());
                kernel.createResource(this, new InterruptEventResource());
                kernel.createResource(this, new InterruptResource());
                kernel.createResource(this, new LoaderEndResource());
                kernel.createResource(this, new MainMemoryResource());
                kernel.createResource(this, new NonExistingResource());
                kernel.createResource(this, new OutputToExternalDeviceResource());
                kernel.createResource(this, new OutputToExternalMemoryResource());
                kernel.createResource(this, new OutputToGeneralMemoryResource());
                kernel.createResource(this, new ProgramPacketResource());
                kernel.createResource(this, new ResourceTemplate());
                kernel.createResource(this, new SystemEndResource());
                kernel.createResource(this, new TaskInExternalMemoryResource());
                kernel.createResource(this, new TaskInSupervisorMemoryResource());
                kernel.createResource(this, new TaskSyntaxCheckedResource());

                // Add resource elements
                // External memory semaphore
                kernel.freeResource(this, new ExternalMemoryResource());
                // General memory semaphore
                kernel.freeResource(this, new GeneralMemoryResource());
                // Memory
                Word address = Word.ZERO;
                Memory mem = RM.getInstance().getMemory();
                try {
                    for (int i = 0; i < Memory.NUM_OF_BLOCKS - RM.NUMBER_OF_GS_BLOCKS; i++) {
                        Word w = mem.getBlock(i);
                        if (w == null) {
                            UserInterface.log("[StartStop] Memory block is null: " + i,3);
                            System.exit(0);
                        }
                        kernel.freeResource(this, new MainMemoryResource(w));
                        address = RM.internalAdd(address, Word.ONE_BLOCK);
                    }
                } catch (InterruptException ex) {
                    UserInterface.log("[StartStop] InterruptException: " + ex,2);
                }

                // Wait for system end resource
                kernel.askForResource(this, "SystemEndResource");
                break;
            case 1:
                // Destroy system processes
                kernel.destroyProcess(this);
                kernel.shutdown();
                break;
        }
    }
}
