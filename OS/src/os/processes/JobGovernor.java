package os.processes;

import java.util.ArrayList;

import os.core.Process;
import os.core.Resource;
import os.rm.*;
import os.resources.*;
import os.processes.*;
import os.ui.UserInterface;

public class JobGovernor extends Process {

    private Word address;

    public JobGovernor() {
        this.externalName = "JobGovernor";
    }

    public JobGovernor(Word address) {
        this();
        this.address = address;
    }

    public void execute() {
        UserInterface.log("Executing " + this,2);
        switch (this.getIC()) {
            case 0:
                kernel.askForResource(this, "MainMemoryResource", RM.BLOCKS_PER_VM);
                break;
            case 1:
                // TODO: free ProgramPacketResource to Loader
                kernel.freeResource(this, new ProgramPacketResource(address, this.ownedResources.get(0)));
                kernel.askForResource(this, "LoaderEndResource");
                break;
            case 2:
                // TODO: create VirtualMachine process
                LoaderEndResource res = (LoaderEndResource) this.ownedResources.get(1);
                kernel.createProcess(this, new VirtualMachine(res.getCS(), res.getDS(), res.getSS(), Word.ZERO.copy(), res.getPTR()));
                kernel.askForResource(this, "InterruptResource");
                break;
            case 3:
                kernel.stopProcess(this.getVM());
                InterruptResource interrupt = (InterruptResource) this.ownedResources.get(this.ownedResources.size() - 1);
                if (interrupt.getType() == "SI" && interrupt.getValue() == 4) {
                    UserInterface.log("[JobGovernor] Output to external memory",2);
                    kernel.freeResource(this, new OutputToExternalMemoryResource((Word) this.getVM().getCurrentParam(2), (Word) this.getVM().getCurrentParam(1)));
                    kernel.askForResource(this, "ExternalWriterEndResource");
                } else if (interrupt.getType() == "SI" && interrupt.getValue() == 3) {
                    UserInterface.log("[JobGovernor] Input from external memory",2);
                    kernel.freeResource(this, new InputFromExternalMemoryResource((Word) this.getVM().getCurrentParam(1)));
                    kernel.askForResource(this, "ExternalReaderEndResource");
                } else if (interrupt.getType() == "SI" && interrupt.getValue() == 2) {
                    UserInterface.log("[JobGovernor] Output to screen",2);
                    UserInterface.log("[JobGovernor] Creating resource Memory: " + this.getVM().getCurrentParam(1),3);
                    kernel.freeResource(this, new OutputToExternalDeviceResource((ArrayList<Word>)this.getVM().getCurrentParam(1)));
                    kernel.askForResource(this, "GetPutDataEndResource");
                } else if (interrupt.getType() == "SI" && interrupt.getValue() == 1) {
                    // TODO: Read From external device
                    UserInterface.log("[JobGovernor] Input from keyboard",2);
                    kernel.freeResource(this, new OutputToExternalDeviceResource(null));
                    kernel.askForResource(this, "GetPutDataEndResource");
                } else if (interrupt.getType() == "SI" && interrupt.getValue() == 7) {
                    UserInterface.log("[JobGovernor] Output to general memory",2);
                    kernel.freeResource(this, new OutputToGeneralMemoryResource((Word) this.getVM().getCurrentParam(1), (Word) this.getVM().getCurrentParam(2)));
                    kernel.askForResource(this, "GeneralWriterEndResource");
                } else if (interrupt.getType() == "SI" && interrupt.getValue() == 6) {
                    UserInterface.log("[JobGovernor] Input from general memory",2);
                    kernel.freeResource(this, new InputFromGeneralMemoryResource((Word) this.getVM().getCurrentParam(1)));
                    kernel.askForResource(this, "GeneralReaderEndResource");
                } else if (interrupt.getType() == "SI" && interrupt.getValue() == 5) {
                    UserInterface.log("[JobGovernor] HALT",2);
                    this.destroyChildren();
                    kernel.freeResource(this, new TaskInExternalMemoryResource(null));
                    kernel.askForResource(this, "NonExistingResource");
                } else if (interrupt.getType() == "TI") {
                    UserInterface.log("[JobGovernor] TI interrupt",2);
                    kernel.askForResource(this, "InterruptResource");
                    this.resetIC();
                } else if (interrupt.getType() == "PI") {
                    UserInterface.log("[JobGovernor] PI was thrown: " + interrupt.getValue() + ", killing VM", 2);
                    this.destroyChildren();
                    kernel.freeResource(this, new TaskInExternalMemoryResource(null));
                    kernel.askForResource(this, "NonExistingResource");
                } else {
                    UserInterface.log("[JobGovernor] Was not able to parse Interrupt: " + interrupt.getType() + "=" + interrupt.getValue() ,2);
                    kernel.askForResource(this, "InterruptResource");
                    this.resetIC();
                }
                kernel.freeResource(this, interrupt);
                break;
            case 4:
                Resource endRes = this.ownedResources.get(this.ownedResources.size() - 1);
                if (endRes.getExternalName() == "GetPutDataEndResource") {
                    ArrayList<Word> input = null;
                    if (null != (input = ((GetPutDataEndResource)endRes).getInput())) {
                        VirtualMachine vm = (VirtualMachine)this.children.get(0);
                        Word address = (Word)this.getVM().getCurrentParam(1);
                        try {
                            for (Word word : input) {
                                vm.setVMWord((short)1, address, word);
                                address = RM.internalAdd(address, Word.ONE);
                            }
                        }
                        catch (InterruptException e) {
                            // TODO: do something with exception
                        }
                    }
                } else if (endRes.getExternalName() == "ExternalWriterEndResource") {

                } else if (endRes.getExternalName() == "ExternalReaderEndResource") {
                    Word w = ((ExternalReaderEndResource) endRes).getReadSingleData();
                    try {
                        this.getVM().pushStack(w);
                    } catch (InterruptException e) {
                        UserInterface.log("[JobGovernor] ExternalReaderEndResource step4: " + e,2);
                    }
                } else if (endRes.getExternalName() == "GeneralWriterEndResource") {

                } else if (endRes.getExternalName() == "GeneralReaderEndResource") {
                    Word w = ((GeneralReaderEndResource) endRes).getResult();
                    try {
                        this.getVM().pushStack(w);
                    } catch (InterruptException e) {
                        UserInterface.log("[JobGovernor] GeneralReaderEndResource step4: " + e,2);
                    }
                } else {
                    UserInterface.log("[JobGovernor] Unknown resource",2);
                }
                this.resetIC();
                kernel.askForResource(this, "InterruptResource");
                kernel.freeResource(this, endRes);
                kernel.activateProcess(this.getVM());
                break;
        }
    }

    public VirtualMachine getVM() {
        UserInterface.log("Current VM JG: " + this.children.get(0),3);
        return (VirtualMachine) this.children.get(0);
    }

    @Override
    public void resetIC() {
        this.setIC(2);
    }
}
