package os.processes;

import java.util.ArrayList;

import os.core.Process;
import os.core.Resource;
import os.rm.Word;
import os.resources.*;
import os.ui.UserInterface;

public class JSC extends Process {

    public JSC() {
        this.externalName = "JSC";
    }

    public void execute() {
        UserInterface.log("[JSC] Executing " + this,2);
        switch (this.IC) {
            case 0:
                kernel.askForResource(this, "TaskInSupervisorMemoryResource");
                break;
            case 1:
                TaskInSupervisorMemoryResource resource = (TaskInSupervisorMemoryResource) this.ownedResources.get(0);
                // Turn into ArrayList<Word> for ease of checking
                ArrayList<Word> words = new ArrayList<Word>();
                for (String line : resource.getProgram().split(System.getProperty("line.separator"))) {
                    Word w = Word.fromString(line);
                    if (w == null) {
                        UserInterface.log("[JSC] Word is null",2);
                    }
                    words.add(w);
                }
                boolean valid = true;
                if (!words.get(0).equals(Word.fromString("DATA"))) {
                    UserInterface.log("[JSC] Does not start with DATA",2);
                    valid = false;
                } else if (!words.get(words.size() - 1).equals(Word.fromString("HALT"))) {
                    UserInterface.log("[JSC] Does not end with HALT",2);
                    valid = false;
                } else {
                    Word code = Word.fromString("CODE");
                    boolean foundCode = false;
                    for(Word w : words) {
                        if (code.equals(w)) {
                            foundCode = true;
                            break;
                        }
                    }
                    valid = foundCode == true;
                }
                // TODO: add more robust syntax checking?

                UserInterface.log("[JSC] JSC program:\n" + resource.getProgram(),2);
                if (valid) {
                    words.add(Word.ZERO); // Add a buffer after the program
                    UserInterface.log("[JSC] Program valid",2);
                    kernel.freeResource(this, new TaskSyntaxCheckedResource(words));
                } else { // program syntax is incorrect
                    UserInterface.log("[JSC] Program syntax is incorrect",2);
                }

                // Cleanup
                kernel.freeResource(this, this.ownedResources.get(0));
                this.resetIC();
                break;
        }
    }
}
