package os.processes;

import os.core.Process;
import os.core.Resource;
import os.resources.*;
import os.ui.UserInterface;

public class MainProc extends Process {

    public MainProc() {
        this.externalName = "MainProc";
    }

    public void execute() {
        UserInterface.log("Executing " + this,2);
        switch (this.getIC()) {
            case 0:
                kernel.askForResource(this, "TaskInExternalMemoryResource");
                break;
            case 1:
                TaskInExternalMemoryResource resource = (TaskInExternalMemoryResource) this.ownedResources.get(0);
                if (resource.getAddress() == null) {
                    // Destroy sender JobGovernor if it signals about job end
                    kernel.destroyProcess(resource.getSender());
                } else {
                    Process governor = new JobGovernor(resource.getAddress());
                    kernel.createProcess(this, governor);
                }
                if (children.size() == 0) {
                    kernel.freeResource(this, new SystemEndResource());
                    kernel.askForResource(this, "NonExistingResource");
                } else {
                    // Cleanup and go to start
                    kernel.freeResource(this, resource);
                    this.resetIC();
                }
                break;
        }
    }
}
