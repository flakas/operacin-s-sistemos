package os.processes;

import java.util.ArrayList;

import os.core.Process;
import os.resources.*;
import os.rm.Word;
import os.rm.InChannel;
import os.rm.OutChannel;
import os.ui.UserInterface;

public class GetPutData extends Process {

    public GetPutData() {
        this.externalName = "GetPutData";
    }

    public void execute() {
        UserInterface.log("Executing " + this,2);
        switch (this.getIC()) {
            case 0:
                kernel.askForResource(this, "OutputToExternalDeviceResource");
                break;
            case 1:
                OutputToExternalDeviceResource res = (OutputToExternalDeviceResource) this.ownedResources.get(0);
                ArrayList<Word> output = (res).getOutput();
                if (output == null) { // If output is empty
                    output = (new InChannel()).exchange();
                    kernel.freeResource(this, new GetPutDataEndResource(output), res.getSender());
                } else { // if out put is empty
                    // TODO: activate OutChannel
                    (new OutChannel()).exchange(output);
                    kernel.freeResource(this, new GetPutDataEndResource(), res.getSender());
                }

                // Cleanup
                kernel.freeResource(this, res);
                this.resetIC();
                break;
        }
    }
}
