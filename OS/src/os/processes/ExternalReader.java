package os.processes;

import os.core.Process;
import os.rm.*;
import os.resources.*;
import os.ui.UserInterface;
import java.util.ArrayList;

public class ExternalReader extends Process {

    public ExternalReader() {
        this.externalName = "ExternalReader";
    }

    public void execute() {
        UserInterface.log("Executing " + this, 2);
        switch (this.getIC()) {
            case 0:
                kernel.askForResource(this, "InputFromExternalMemoryResource");
                break;
            case 1:
                InputFromExternalMemoryResource res = (InputFromExternalMemoryResource) this.ownedResources.get(0);
                ExternalMemory exMem = RM.getInstance().getExternalMemory();
                Word addr = res.getAddress();
                Word word;
                ArrayList<Word> readWords = new ArrayList<Word>();
                while (!Word.ZERO.equals(word = exMem.getWord(addr))) {
                    readWords.add(word);
                    addr = RM.internalAdd(addr, Word.ONE);
                }
                kernel.freeResource(this, new ExternalReaderEndResource(readWords), res.getSender());

                // Cleanup
                kernel.freeResource(this, res);
                this.resetIC();
                break;
        }
    }
}
