package os.core;

import os.core.Process;

public class WaitingProcess {
    private Process process;
    private int amount;

    public WaitingProcess(Process process, int amount) {
        this.process = process;
        this.amount = amount;
    }

    public Process getProcess() {
        return this.process;
    }

    public int getAmount() {
        return this.amount;
    }
}
