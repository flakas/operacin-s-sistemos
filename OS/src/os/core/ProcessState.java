package os.core;

public enum ProcessState {
    RUN,
    READY,
    BLOCKED,
    READYSTOPPED,
    BLOCKEDSTOPPED
}
