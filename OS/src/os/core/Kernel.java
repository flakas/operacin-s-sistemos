package os.core;

import os.rm.RM;
import os.processes.*;
import os.ui.UserInterface;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.PriorityQueue;


public class Kernel {

    private ArrayList<Resource> resources = new ArrayList<Resource>();

    private ArrayList<Process> allProcesses = new ArrayList<Process>();
    private PriorityQueue<Process> readyProcesses = new PriorityQueue<Process>();
    private PriorityQueue<Process> blockedProcesses = new PriorityQueue<Process>();
    private Process currentProcess;

    private static Kernel kernel = null;

    private boolean shutdown = false;

    public Kernel() {
    }

    public void start() {
        Process startStopProc = new StartStop();
        this.createProcess(null, startStopProc);
        planner();
    }

    public void createProcess(Process parent, Process newProc) {
        UserInterface.log("Creating process " + newProc + " -- START", 1);
        this.allProcesses.add(newProc);
        this.readyProcesses.add(newProc);
        newProc.setState(ProcessState.READY);
        if (parent != null) {
            parent.addChild(newProc);
            newProc.setParent(parent);
        }
        UserInterface.log("Creating process " + newProc + " -- END", 1);
    }

    public void destroyProcess(Process process) {
        UserInterface.log("Destroying process " + process + " -- START", 1);
        // Remove from parent
        if (process.getParent() != null) {
            process.getParent().removeChild(process);
        }
        // Cleanup children and resources
        process.destroyChildren();
        process.releaseResources();
        process.destroyResources();

        // Remove from any lists - just forget about it
        allProcesses.remove(process);
        readyProcesses.remove(process);
        blockedProcesses.remove(process);
        if (process.getWaitingForResource() != null) {
            process.getWaitingForResource().removeWaitingProc(process);
        }

        // TODO: remove from resource waiting lists if process is waiting
        UserInterface.log("Destroying process " + process + " -- END", 1);
    }

    public void activateProcess(Process process) {
        // TODO: activateProcess should change process state
        // READYSTOPPED -> READY
        // BLOCKEDSTOPPED -> BLOCKED
        // and should it call planner?

        UserInterface.log("Activating process " + process, 1);
        switch (process.getState()) {
            case BLOCKED:
                process.setState(ProcessState.BLOCKED);
                break;
            case READY:
                process.setState(ProcessState.READY);
                break;
        }
        planner(); // TODO: call planner?
    }

    public void stopProcess(Process process) {
        UserInterface.log("Stopping process " + process, 1);
        switch (process.getState()) {
            case BLOCKED:
                process.setState(ProcessState.BLOCKEDSTOPPED);
                break;
            case READY:
                process.setState(ProcessState.READYSTOPPED);
                break;
        }
        planner(); // TODO: call planner?
    }

    public void runProcess(Process process) {
        process.setState(ProcessState.RUN);
        this.readyProcesses.remove(process);
        this.currentProcess = process;
    }

    public void createResource(Process owner, Resource newRes) {
        UserInterface.log("Creating resource " + newRes, 1);
        newRes.setCreator(owner);
        owner.addCreatedResource(newRes);
        newRes.setStatus(0);
        this.resources.add(newRes);
    }

    public void deleteResource(Process process, Resource resource) {
        UserInterface.log("Deleting resource " + resource + " from " + process + " -- START", 1);
        this.resources.remove(resource);
    }

    public void freeResource(Process process, String resource) {
        UserInterface.log("Freeing resource " + resource + " from " + process + " -- START", 1);
        Resource res = this.findResourceByExName(resource);
        process.releaseResource(res);
        res.setStatus(0);
        res.distribute();
        planner();
    }

    public void freeResource(Process process, Resource element) {
        UserInterface.log("Freeing resource " + element + " from " + process + " -- START", 1);
        // Find resource descriptor
        Resource res = this.findResourceByExName(element.getExternalName());
        element.setSender(process);
        // Add element to the descriptor
        if (res.isReusable() || (!res.isReusable() && !element.wasUsed())) {
            res.release(element);
        }
        // Remove element from the process if it has one
        if (process != null) {
            process.releaseResource(element);
        }
        res.distribute();
        planner();
    }

    public void freeResource(Process process, Resource element, Process receiver) {
        element.setReceiver(receiver);
        this.freeResource(process, element);
    }

    public void freeResource(Process process, String resource, Process receiver) {
        UserInterface.log("Freeing resource " + resource + " from " + process + " -- START", 1);
        Resource res = this.findResourceByExName(resource);
        process.releaseResource(res);
        res.setStatus(0);
        planner();
    }

    public void askForResource(Process procAsked, String externalName) {
        this.askForResource(procAsked, externalName, 1);
    }

    public void askForResource(Process procAsked, String externalName, int amount) {
        UserInterface.log(procAsked + " asked for resource: " + externalName, 1);
        Resource res = this.findResourceByExName(externalName);
        procAsked.setState(ProcessState.BLOCKED);
        procAsked.setWaitingForResource(res);
        res.addWaitingProc(procAsked, amount);
        res.distribute();
        planner();
    }

    public void giveResource(Process process, Resource resource) {
        Resource res = this.findResourceByExName(resource.getExternalName());
        res.removeElement(resource);
        if (!resource.isReusable()) {
            resource.setUsed();
        }
        res.removeWaitingProc(process);
        process.addOwnedResource(resource);
        process.setWaitingForResource(null);
        if (process == this.getCurrentProcess()) {
            process.setState(ProcessState.RUN);
        } else {
            process.setState(ProcessState.READY);
            this.blockedProcesses.remove(process);
            this.readyProcesses.add(process);
        }
    }

    public Resource findResourceByExName(String exName) {
        for (Resource resource : this.resources) {
            if (resource.getExternalName().equals(exName)) {
                return resource;
            }
        }
        UserInterface.log("ERROR: asking for unexisting resource: " + exName, 1);
        this.shutdown();
        return null;
    }

    public void planner() {
        Process firstReady = this.readyProcesses.peek();
        Process current = this.getCurrentProcess();
        if (current != null && current.getState() != ProcessState.RUN) {
            this.blockedProcesses.add(current);
            this.currentProcess = null;
            current = null;
        }
        if (current == null) {
            if (firstReady == null) {
                UserInterface.log("All processes are blocked, doing nothing", 1);
            } else {
                UserInterface.log("[Planner] Changing process", 1);
                firstReady = this.readyProcesses.poll();
                firstReady.setState(ProcessState.RUN);
                this.runProcess(firstReady);
            }
        } else {
            if (firstReady == null) {
                UserInterface.log("[Planner] No other ready processes, staying with current one", 1);
            } else if (firstReady.priority > current.priority) {
                UserInterface.log("[Planner] Current process has lower priority", 1);
                firstReady = this.readyProcesses.poll();
                firstReady.setState(ProcessState.RUN);
                this.runProcess(firstReady);
                current.setState(ProcessState.READY);
                this.readyProcesses.add(current);
            }
        }
    }

    public Process getCurrentProcess() {
        return this.currentProcess;
    }

    public static Kernel getInstance() {
        if (kernel == null) {
            kernel = new Kernel();
        }
        return kernel;
    }

    public void run() {
        Process p = this.getCurrentProcess();
        if (p == null) {
            UserInterface.log("No processes are running", 1);
        } else {
            p.run();
        }
    }

    public void shutdown() {
        this.shutdown = true;
    }

    public boolean continueRunning() {
        return this.getCurrentProcess() != null && !this.shutdown;
    }

    public Iterator<Process> getAllProcesses() {
        return this.allProcesses.iterator();
    }

    public Iterator<Process> getReadyProcesses() {
        return this.readyProcesses.iterator();
    }

    public Iterator<Process> getBlockedProcesses() {
        return this.blockedProcesses.iterator();
    }

    public Iterator<Resource> getAllResources() {
        return this.resources.iterator();
    }
}
