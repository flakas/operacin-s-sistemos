package os.core;

import os.ui.UserInterface;
import java.util.ArrayList;

public abstract class Resource {

    private int ID;
    private static int IDs = 0;

    protected Process creator;

    protected ArrayList<WaitingProcess> waitingProcs = new ArrayList<WaitingProcess>();
    protected ArrayList<Resource> resourceElements = new ArrayList<Resource>();

    protected String externalName;

    /* Status of the resource: 0 - free, 1 - blocked, 2 - owned */
    private int status = 1;

    protected boolean reusable;
    protected boolean wasUsed;

    protected Process sender;
    protected Process receiver;

    protected static Kernel kernel;

    public Resource() {
        this.ID = (IDs++);
        if (kernel == null) {
            kernel = Kernel.getInstance();
        }
        this.reusable = false;
    }

    public void release(Resource res) {
        this.resourceElements.add(res);
    }

    public void distribute() {
        this.distributeAny();
    }

    protected void distributeAny() {
        for (int i = 0; i < this.resourceElements.size(); i += 1) {
            if (this.waitingProcs.size() > 0) {
                WaitingProcess wp = this.waitingProcs.get(0);
                Resource element = this.resourceElements.get(0);
                kernel.giveResource(wp.getProcess(), element);
            } else {
                break;
            }
        }
    }

    protected void distributeToReceivers() {
        int currentIndex = 0;
        boolean gotResource = false;
        for (int i = 0; i < this.waitingProcs.size(); i += 1) {
            gotResource = false;
            WaitingProcess wp = this.waitingProcs.get(currentIndex);
            for (int j = 0; j < this.resourceElements.size(); j += 1) {
                if (wp.getProcess() == this.resourceElements.get(j).getReceiver()) {
                    Resource element = this.resourceElements.get(j);
                    kernel.giveResource(wp.getProcess(), element);
                    gotResource = true;
                    break;
                }
            }
            if (!gotResource) {
                currentIndex += 1;
            }
        }
    }

    public void removeElement(Resource element) {
        boolean removed = this.resourceElements.remove(element);
        if (!removed) {
            UserInterface.log("[Resource] Failed to remove resource element " + element,2);
        }
    }

    public Process getSender() {
        return this.sender;
    }

    public void setSender(Process sender) {
        this.sender = sender;
    }

    public Process getReceiver() {
        return this.receiver;
    }

    public void setReceiver(Process receiver) {
        this.receiver = receiver;
    }

    public String getExternalName() {
        return this.externalName;
    }

    public int getID() {
        return this.ID;
    }

    public boolean isReusable() {
        return this.reusable;
    }

    public boolean wasUsed() {
        return this.wasUsed;
    }

    public void setUsed() {
        this.wasUsed = true;
    }

    public void setCreator(Process creator) {
        this.creator = creator;
    }

    public void addWaitingProc(Process proc) {
        this.addWaitingProc(proc, 1);
    }

    public void addWaitingProc(Process proc, int amount) {
        this.waitingProcs.add(new WaitingProcess(proc, amount));
    }

    public void removeWaitingProc(Process proc) {
        for (int i = 0; i < this.waitingProcs.size(); i += 1) {
            if (this.waitingProcs.get(i).getProcess() == proc) {
                this.waitingProcs.remove(i);
            }
        }
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String toString() {
        return this.getExternalName() + "[" + this.getID() + "]";
    }

    public void removeElements() {
        this.resourceElements.clear();
    }

    public int totalFreeElements() {
        return this.resourceElements.size();
    }

    public int totalWaitingProcesses() {
        return this.waitingProcs.size();
    }

    public ArrayList<WaitingProcess> getWaitingProcesses() {
        return this.waitingProcs;
    }
}
