package os.core;

import os.core.*;
import os.rm.RM;
import os.ui.UserInterface;
import java.util.ArrayList;

public abstract class Process implements Comparable {

    protected int ID;
    protected static int IDs = 0;

    protected ArrayList<Resource> createdResources = new ArrayList<Resource>();
    protected ArrayList<Resource> ownedResources = new ArrayList<Resource>();

    protected Resource waitingForResource;

    protected ProcessState processState;

    protected Process parent;

    protected ArrayList<Process> children = new ArrayList<Process>();

    protected int priority = 1;

    protected String externalName;

    protected int IC = 0;

    protected static Kernel kernel;
    protected static RM rm;

    public Process() {
        this.ID = (IDs++);
        if (kernel == null) {
            kernel = Kernel.getInstance();
        }
        if (rm == null) {
            rm = RM.getInstance();
        }
    }

    public void run() {
        execute();
        this.IC += 1;
    }

    public abstract void execute();

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public void destroyChildren() {
        for(int i = this.children.size() - 1; i >= 0; i -= 1) {
            kernel.destroyProcess(this.children.get(i));
        }
    }

    public boolean removeChild(Process p) {
        if (this.children.size() > 0) {
            return this.children.remove(p);
        } else {
            return true;
        }
    }

    public void releaseResources() {
        for(int i = this.ownedResources.size() - 1; i >= 0; i -= 1) {
            if (this.ownedResources.get(i).getExternalName() == "MainMemoryResource") {
                UserInterface.log("[Process] " + this + " trying to release MainMemoryResource", 3);
            }
            kernel.freeResource(this, this.ownedResources.get(i));
        }
    }

    public void destroyResources() {
        for(int i = this.createdResources.size() - 1; i >= 0; i -= 1) {
            kernel.deleteResource(this, this.createdResources.get(i));
        }
    }

    public void addOwnedResource(Resource resource) {
        this.ownedResources.add(resource);
    }

    public void releaseResource(Resource resource) {
        this.ownedResources.remove(resource);
    }

    public int getPriority() {
        return this.priority;
    }

    public Process getParent() {
        return this.parent;
    }

    public void setParent(Process parent) {
        this.parent = parent;
    }

    public void addChild(Process child) {
        this.children.add(child);
    }

    public void addCreatedResource(Resource resource) {
        this.createdResources.add(resource);
    }

    public String getExternalName() {
        return this.externalName;
    }

    public int getID() {
        return this.ID;
    }

    public int getIC() {
        return this.IC;
    }

    public void resetIC() {
        this.IC = -1;
    }

    protected void setIC(int ic) {
        this.IC = ic;
    }

    @Override
    public int compareTo(Object o) {
        return ((Integer)(((Process) o).priority)).compareTo(((Integer)this.priority));
    }

    public String toString() {
        return this.getExternalName() + "[" + this.getID() + "]";
    }

    public ProcessState getState() {
        return this.processState;
    }

    public void setState(ProcessState state) {
        this.processState = state;
    }

    public Resource getWaitingForResource() {
        return this.waitingForResource;
    }

    public void setWaitingForResource(Resource res) {
        this.waitingForResource = res;
    }
}
