package os.ui;

import java.util.Iterator;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.ByteBuffer;

import os.core.Kernel;
import os.core.Process;
import os.core.Resource;
import os.core.WaitingProcess;
import os.resources.*;
import os.rm.RM;

public class UserInterface {
    private static UserInterface ui;
    private boolean exit = false;
    private RM rm;
    private Kernel kernel;

    public static final int LOG_LEVEL = 3;

    public UserInterface() {
        this.rm = RM.getInstance();
        this.kernel = Kernel.getInstance();
    }

    public void start() {
        this.rm.loadOS();
    }

    public void run() {
        while (!this.exit) {
            this.executeCommand(this.getCommand());
        }
    }

    public String getCommand() {
        InputStreamReader is = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(is);
        System.out.print(">>> ");
        try {
            String command = br.readLine();
            return command;
        } catch (IOException ex) {
            System.out.println("User interface IOException:");
            System.out.println(ex);
        }

        return "";
    }

    public void executeCommand(String command) {
        if (command.equals("step") || command.equals("s")) {
            this.step();
        } else if (command.startsWith("step")) {
            this.stepTimes(command.substring(4));
        } else if (command.equals("run")) {
            this.runOS();
        } else if (command.startsWith("load")) {
            this.load(command.substring(4).trim());
        } else if (command.equals("current")) {
            this.current();
        } else if (command.equals("processes")) {
            this.processes();
        } else if (command.equals("ready")) {
            this.ready();
        } else if (command.equals("blocked")) {
            this.blocked();
        } else if (command.equals("resources")) {
            this.resources();
        } else if (command.equals("memory")) {
            this.memory();
        } else if (command.equals("help")) {
            this.help();
        } else if (command.equals("exit") || command.equals("quit")) {
            System.out.println("Exiting...");
            this.exit = true;
        } else {
            System.out.println("Unrecognized command: " + command);
        }
    }

    private void step() {
        if (this.kernel.continueRunning()) {
            this.rm.stepOS();
        } else {
            System.out.println("OS cannot do anything");
        }
    }

    private void stepTimes(String timesStr) {
        int times = 0;
        try {
            times = Integer.parseInt(timesStr);
        }
        catch (NumberFormatException e) {
            System.out.println("Wrong number entered");
            return ;
        }
        for (int i = 0; i < times; i ++) {
            this.step();
        }
    }

    private void runOS() {
        while (this.kernel.continueRunning()) {
            this.rm.stepOS();
        }
    }

    private void load(String filename) {
        System.out.println("Trying to load: " + filename);
        try {
            String contents = readFile(filename, Charset.defaultCharset());
            // TODO: Pass program contents along as well
            if (contents.length() > 0) {
                kernel.freeResource(null, new TaskInSupervisorMemoryResource(contents));
            } else {
                System.out.println("Program file is empty");
            }
        } catch (IOException ex) {
            System.out.println("Cannot load file: " + filename);
            System.out.println(ex);
        }
    }

    private void current() {
        System.out.println("Current process: ");
        Process currentProcess = this.kernel.getCurrentProcess();
        if (currentProcess == null) {
            System.out.println("Current process is not set");
        } else {
            System.out.println(currentProcess);
        }
    }

    private void processes() {
        System.out.println("All processes: ");
        int total = 0;
        Iterator<Process> procIter = this.kernel.getAllProcesses();
        while (procIter.hasNext()) {
            Process p = procIter.next();
            total += 1;
            System.out.println(p);
        }
        System.out.println("Total processes: " + total);
    }

    private void ready() {
        System.out.println("Ready processes: ");
        int total = 0;
        Iterator<Process> procIter = this.kernel.getReadyProcesses();
        while (procIter.hasNext()) {
            Process p = procIter.next();
            total += 1;
            System.out.println(p);
        }
        System.out.println("Total processes: " + total);
    }

    private void blocked() {
        System.out.println("Blocked processes: ");
        int total = 0;
        Iterator<Process> procIter = this.kernel.getBlockedProcesses();
        while (procIter.hasNext()) {
            Process p = procIter.next();
            total += 1;
            System.out.println(p);
        }
        System.out.println("Total processes: " + total);
    }

    private void resources() {
        System.out.println("All resources: ");
        int total = 0;
        Iterator<Resource> resIter = this.kernel.getAllResources();
        while (resIter.hasNext()) {
            Resource r = resIter.next();
            total += 1;
            System.out.print(r + "[" + r.totalFreeElements() + "/" + r.totalWaitingProcesses() + "]");
            if (r.totalWaitingProcesses() > 0) {
                System.out.print(": ");
                for (WaitingProcess wp : r.getWaitingProcesses()) {
                    System.out.print(wp.getProcess() + ", ");
                }
            }
            System.out.println("");
        }
        System.out.println("Total resources: " + total);
    }

    private void memory() {
        System.out.println("User memory: ");
        System.out.println(RM.getInstance().getMemory());
    }

    private void help() {
        System.out.println("============= HELP =============\n");
        System.out.println("Commands:\n"
                + "(s)tep - allow the OS to do a single step\n"
                + "step{n} - allow the OS to do n steps\n"
                + "current - display current process information\n"
                + "processes - all processes\n"
                + "load filename - load program from file\n"
                + "ready - all ready processes\n"
                + "blocked - all blocked processes\n"
                + "resources - all resoures\n"
                + "memory - show memory\n"
                + "help - this help page\n"
                + "exit - exit OS\n");
    }

    static String readFile(String path, Charset encoding) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return encoding.decode(ByteBuffer.wrap(encoded)).toString();
    }


    public static void log(String message, int level) {
        if (level <= UserInterface.LOG_LEVEL) {
            System.out.println(message);
        }
    }
}
