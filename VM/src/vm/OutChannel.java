package vm;

public class OutChannel extends Channel {
    public void exchange() {
        RM rm = RM.getInstance();
        VM vm = rm.getCurrentVM();
        short b = -1;
        Word vmAddress = vm.getCurrentParam(1);
        Word w;


        try {
            do {
                w = vm.getVMWord((short)1, vmAddress);
                for (int i = 0; i < Memory.WORD_SIZE && b != 0; i++) {
                    b = w.getByte(i);
                    System.out.print((char)b);
                }
                vmAddress = RM.internalAdd(vmAddress, Word.ONE);
            } while (b != 0);
        }
        catch (InterruptException e) {
            // Ignore any interrupts during supervisor mode
        }
        rm.setIOI((byte)2);
    }
}
