package vm;

public class Word implements Comparable<Word> {
    public static final int WORD_SIZE = 4;
    public static final Word ZERO = new Word(new short[]{0, 0, 0, 0});
    public static final Word ONE = new Word(new short[]{0, 0, 0, 1});
    private short[] bytes = new short[WORD_SIZE];

    public Word () {

    }

    public Word (short[] bytes) { 
        if (bytes.length != WORD_SIZE) {
            throw new RuntimeException("Trying to create Word with wrong length");
        }
        this.bytes = bytes;
    }

    public Word (String bytes) {
        if (bytes.replaceAll(" ", "").length() != this.bytes.length * 2) {
            throw new RuntimeException("Trying to create Word with wrong length");
        }
        try {
            long num = Long.parseLong(bytes.replaceAll(" ",""), 16);
            for (int i = Word.WORD_SIZE - 1; i >= 0; i -= 1) {
                this.setByte(i, (short)(num % RM.NUMERAL_SYSTEM));
                num /= RM.NUMERAL_SYSTEM;
            }
        }
        catch (NumberFormatException e) {
            throw new RuntimeException("Cannot create a new word from string: " + bytes);
        }
    }

    public short getByte(int index) {
        if (index >= WORD_SIZE) {
            throw new RuntimeException("Index not found, trying to get " + index + " on a " + this.bytes.length + "size Word"); 
        }
        return this.bytes[index];
    }

    public void setByte(int index, short value) {
        if (index >= WORD_SIZE) {
            throw new RuntimeException("Index not found, trying to set " + index + " on a " + this.bytes.length + "size Word"); 
        }
        if (value >= RM.NUMERAL_SYSTEM) {
            throw new RuntimeException("Numeral system mismatch when setting a new Word");
        }
        this.bytes[index] = value;
    }

    public short[] toByteArray() {
        return this.bytes.clone();
    }

    public long toLong() {
        long res = 0;
        int j = 0;
        long num;
        for (int i = WORD_SIZE - 1; i >= 0; i -= 1) {
            num = this.bytes[i];
            for (int k = 0; k < j; k++) {
                num *= RM.NUMERAL_SYSTEM;
            }
            res += num;
            j += 1;
        }
        return res;
    }

    public static Word parseLong(long num) {
        Word result = new Word();
        for (int i = Word.WORD_SIZE - 1; i >= 0; i -= 1) {
            result.setByte(i, (short)(num % RM.NUMERAL_SYSTEM));
            num /= RM.NUMERAL_SYSTEM;
        }
        return result;
    }

    public static short getLeftNibble(short b) {
        return (short)((b >> 4) & 0x0f);
    }

    public static short getRightNibble(short b) {
        return (short)(b & 0x0f);
    }

    @Override
    public int compareTo(Word o) {
        short[] oBytes = o.toByteArray();
        for (int i = 0; i < WORD_SIZE; i++) {
            if (oBytes[i] < this.bytes[i]) {
                return 1;
            }
            else if (oBytes[i] > this.bytes[i]) {
                return -1;
            }
        }
        return 0;
    }

    @Override
    public int hashCode() {
        short sum = 0;
        for (short b : this.bytes) {
            sum += b;
        }
        return sum;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Word)) {
            throw new ClassCastException("Word object expected");
        }
        Word otherWord = (Word) o;
        short[] oBytes = otherWord.toByteArray();
        for (int i = 0; i < WORD_SIZE; i++) {
            if (this.bytes[i] != oBytes[i]) {
                return false;
            }
        }
        return true;
    }

    public Word copy() {
        return new Word(this.toByteArray());
    }

    @Override
    public String toString() {
        String ret = "";
        for (short b : this.bytes) {
                ret += String.format("%02x ", b);
//                ret += String.format("%2c ", b);
        }
        return ret;
    }

    public String toStringSymbols() {
        String ret = "";
        for (short b : this.bytes) {
                ret += String.format("%2c ", b);
        }
        return ret;
    }
}
