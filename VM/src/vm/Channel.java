package vm;

public abstract class Channel {
    public abstract void exchange();
}
