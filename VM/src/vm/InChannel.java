package vm;

import java.io.DataInputStream;
import java.io.IOException;

public class InChannel extends Channel {
    public void exchange() {
        short[] wordBuf = new short[Memory.WORD_SIZE];
        for (int i = Memory.WORD_SIZE - 1; i >= 0; i -= 1) {
            wordBuf[i] = 0;
        }
        int i = 0;
        byte b;
        Word word;
        RM rm = RM.getInstance();
        VM vm = rm.getCurrentVM();
        Word vmAddress = vm.getCurrentParam(1);

        DataInputStream stream = new DataInputStream(System.in);
        try
        {
            do
            {
                b = stream.readByte();
                if (b != '\n' && b != 0) {
                    wordBuf[i++] = b;
                }
                if (i == Memory.WORD_SIZE || b == '\n' || b == 0) {
                    i = 0;
                    word = new Word(wordBuf);
                    if (!vm.setVMWord((short)1, vmAddress, word)) {
                        throw new RuntimeException("[RM] Input error - cannot set VM address " + vmAddress);
                    }
                    vmAddress = RM.internalAdd(vmAddress, Word.ONE);
                    wordBuf = new short[4];
                }
            } while (b != '\n' && b != 0);
        }
        catch (InterruptException e) {
            // Ignore any interrupts during supervisor mode
        }
        catch (IOException e)
        {
            System.out.println("[RM] Input error in InChannel.");
        }
        rm.setIOI((byte)1);
    }
}
