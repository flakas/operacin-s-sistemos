package vm.gui;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;
import javax.swing.JOptionPane;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import vm.*;

public class Controls extends JPanel {
    private JRadioButton step;
    private JRadioButton normal;

    private JButton stepBut;
    private JButton loadBut;
    private JButton startBut;
    private JButton realMemoryBut;
    private JButton vmMemoryBut;

    public Controls(LayoutManager l) {
        super(l);
        GridBagConstraints c = new GridBagConstraints();

        ButtonGroup group = new ButtonGroup();

        step = new JRadioButton("Step mode");
        c.gridx = 0;
        c.gridy = 0;
        group.add(step);
        step.setActionCommand("step");
        add(step, c);
        step.addActionListener(new StepNormalAction());

        normal = new JRadioButton("Normal mode");
        c.gridx = 0;
        c.gridy = 1;
        group.add(normal);
        normal.setSelected(true);
        normal.setActionCommand("normal");
        add(normal, c);
        normal.addActionListener(new StepNormalAction());

        c.gridx = 1;
        c.gridy = 0;
        c.gridheight = 2;
        c.ipadx = 20;
        c.ipady = 20;
        stepBut = new JButton("Step");
        add(stepBut, c);
        stepBut.addActionListener(new StepAction());

        c.gridx = 2;
        c.gridy = 0;
        c.ipadx = 20;
        c.ipady = 20;
        loadBut = new JButton("Load program");
        add(loadBut, c);
        loadBut.addActionListener(new LoadAction());

        c.gridx = 3;
        c.gridy = 0;
        c.ipadx = 20;
        c.ipady = 20;
        startBut = new JButton("Start");
        add(startBut, c);
        startBut.addActionListener(new StartAction());

        c.gridx = 4;
        c.gridy = 0;
        c.ipadx = 20;
        c.ipady = 20;
        realMemoryBut = new JButton("View real memory");
        add(realMemoryBut, c);
        realMemoryBut.addActionListener(new RealMemoryAction());

        c.gridx = 5;
        c.gridy = 0;
        c.ipadx = 20;
        c.ipady = 20;
        vmMemoryBut = new JButton("View VM memory");
        add(vmMemoryBut, c);
        vmMemoryBut.addActionListener(new VMMemoryAction());
        startingDisable();
    }

    private void startingDisable() {
        step.setEnabled(false);
        normal.setEnabled(false);
        stepBut.setEnabled(false);
        loadBut.setEnabled(true);
        startBut.setEnabled(false);
        realMemoryBut.setEnabled(false);
        vmMemoryBut.setEnabled(false);
    }

    public void setEnabled(boolean enable) {
        this.step.setEnabled(enable);
        this.normal.setEnabled(enable);
        this.stepBut.setEnabled(enable);
        this.loadBut.setEnabled(enable);
        //this.startBut.setEnabled(enable);
        this.realMemoryBut.setEnabled(enable);
        this.vmMemoryBut.setEnabled(enable);
    }

    private class StepNormalAction implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getActionCommand().equals("step")) {
                Controls.this.startBut.setEnabled(false);
                Controls.this.stepBut.setEnabled(true);
            }
            else if (e.getActionCommand().equals("normal")) {
                Controls.this.startBut.setEnabled(true);
                Controls.this.stepBut.setEnabled(false);
            }
        }
    }

    private class StepAction implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            Thread th = new Thread() {
                public void run() {
                    try {
                        if (!RM.getInstance().step()) {
                            Controls.this.startingDisable();
                            Window.getInstance().loadRegisters();
                            RM.getInstance().resetInterrupts();
                        }
                        else {
                            Window.getInstance().loadRegisters();
                        }
                    }
                    catch (Exception exc) {
                        Window.getInstance().showError(exc.toString());
                    }
                }
            };
            th.start();
        }
    }

    private class LoadAction implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            RM rm = RM.getInstance();
            String addrStr = JOptionPane.showInputDialog("Enter address in external memory");
            Word address = null;
            if (addrStr != null) {
                try {
                    address = new Word(addrStr);
                    rm.createVM();
                    rm.loadProgramToVM(address);
                    Window.getInstance().loadRegisters();
                    Window.getInstance().loadMemory();
                    Controls.this.step.setEnabled(true);
                    Controls.this.normal.setEnabled(true);
                    Controls.this.startBut.setEnabled(true);
                    Controls.this.realMemoryBut.setEnabled(true);
                    Controls.this.vmMemoryBut.setEnabled(true);
                }
                catch (Exception exc) {
                    Window.getInstance().showError(exc.toString());
                }
            }
        }
    }

    private class StartAction implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            RM rm = RM.getInstance();
            Window.getInstance().setEnabled(false);
            Thread th = new Thread() {
                public void run() {
                    try {
                        Controls.this.startBut.setEnabled(false);
                        RM.getInstance().run();
                        Controls.this.startingDisable();
                        Window.getInstance().loadRegisters();
                        RM.getInstance().resetInterrupts();
                    }
                    catch (Exception exc) {
                        Window.getInstance().showError(exc.toString());
                    }
                }
            };
            th.start();
        }
    }

    private class RealMemoryAction implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            Window.getInstance().getRealMemoryWindow().update();
            Window.getInstance().getRealMemoryWindow().makeVisible();
        }
    }

    private class VMMemoryAction implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            Window.getInstance().getVirtualMemoryWindow().update();
            Window.getInstance().getVirtualMemoryWindow().makeVisible();
        }
    }
 
}
