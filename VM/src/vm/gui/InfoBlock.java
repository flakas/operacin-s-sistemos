package vm.gui;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import vm.*;

public class InfoBlock extends JPanel {
    private JTextField command;
    private JTextField si;
    private JTextField ioi;
    private JTextField ti;
    private JTextField pi;
    private JTextField ip;
    private JTextField sp;
    private JTextField ptr;
    private JTextField mode;
    private JTextField cs;
    private JTextField ds;
    private JTextField ss;
    private JTextField gs;
    private JTextField sf;
    private JButton applyBut;

    public InfoBlock(LayoutManager l) {
        super(l);
        GridBagConstraints c = new GridBagConstraints();

        JLabel label = new JLabel("Command");
        c.gridx = 0;
        c.gridy = 0;
        c.ipadx = 20;
        c.ipady = 20;
        add(label, c);
        command = new JTextField(9);
        c.gridx = 1;
        c.gridy = 0;
        c.ipadx = 0;
        c.ipady = 0;
        command.setEnabled(false);
        add(command, c);


        JLabel title = new JLabel("Registers", JLabel.CENTER);
        c.gridx = 0;
        c.gridy = 1;
        c.gridwidth = 2;
        c.ipadx = 20;
        c.ipady = 20;
        add(title, c);
        c.gridwidth = 1;


        label = new JLabel("SI");
        c.gridx = 0;
        c.gridy = 4;
        c.ipadx = 20;
        c.ipady = 20;
        add(label, c);
        si = new JTextField(9);
        c.gridx = 1;
        c.gridy = 4;
        c.ipadx = 0;
        c.ipady = 0;
        si.setEnabled(false);
        add(si, c);
        label = new JLabel("IOI");
        c.gridx = 0;
        c.gridy = 5;
        c.ipadx = 20;
        c.ipady = 20;
        add(label, c);
        ioi = new JTextField(9);
        c.gridx = 1;
        c.gridy = 5;
        c.ipadx = 0;
        c.ipady = 0;
        ioi.setEnabled(false);
        add(ioi, c);
        label = new JLabel("TI");
        c.gridx = 0;
        c.gridy = 7;
        c.ipadx = 20;
        c.ipady = 20;
        add(label, c);
        ti = new JTextField(9);
        c.gridx = 1;
        c.gridy = 7;
        c.ipadx = 0;
        c.ipady = 0;
        add(ti, c);
        label = new JLabel("PI");
        c.gridx = 0;
        c.gridy = 6;
        c.ipadx = 20;
        c.ipady = 20;
        add(label, c);
        pi = new JTextField(9);
        c.gridx = 1;
        c.gridy = 6;
        c.ipadx = 0;
        c.ipady = 0;
        pi.setEnabled(false);
        add(pi, c);

        label = new JLabel("IP");
        c.gridx = 0;
        c.gridy = 8;
        c.ipadx = 20;
        c.ipady = 20;
        add(label, c);
        ip = new JTextField(9);
        c.gridx = 1;
        c.gridy = 8;
        c.ipadx = 0;
        c.ipady = 0;
        add(ip, c);

        label = new JLabel("SP");
        c.gridx = 0;
        c.gridy = 9;
        c.ipadx = 20;
        c.ipady = 20;
        add(label, c);
        sp = new JTextField(9);
        c.gridx = 1;
        c.gridy = 9;
        c.ipadx = 0;
        c.ipady = 0;
        add(sp, c);

        label = new JLabel("PTR");
        c.gridx = 0;
        c.gridy = 11;
        c.ipadx = 20;
        c.ipady = 20;
        add(label, c);
        ptr = new JTextField(9);
        c.gridx = 1;
        c.gridy = 11;
        c.ipadx = 0;
        c.ipady = 0;
        ptr.setEnabled(false);
        add(ptr, c);

        label = new JLabel("Mode");
        c.gridx = 0;
        c.gridy = 2;
        c.ipadx = 20;
        c.ipady = 20;
        add(label, c);
        mode = new JTextField(9);
        c.gridx = 1;
        c.gridy = 2;
        c.ipadx = 0;
        c.ipady = 0;
        mode.setEnabled(false);
        add(mode, c);

        label = new JLabel("CS");
        c.gridx = 0;
        c.gridy = 12;
        c.ipadx = 20;
        c.ipady = 20;
        add(label, c);
        cs = new JTextField(9);
        c.gridx = 1;
        c.gridy = 12;
        c.ipadx = 0;
        c.ipady = 0;
        cs.setEnabled(false);
        add(cs, c);

        label = new JLabel("DS");
        c.gridx = 0;
        c.gridy = 13;
        c.ipadx = 20;
        c.ipady = 20;
        add(label, c);
        ds = new JTextField(9);
        c.gridx = 1;
        c.gridy = 13;
        c.ipadx = 0;
        c.ipady = 0;
        ds.setEnabled(false);
        add(ds, c);

        label = new JLabel("SS");
        c.gridx = 0;
        c.gridy = 14;
        c.ipadx = 20;
        c.ipady = 20;
        add(label, c);
        ss = new JTextField(9);
        c.gridx = 1;
        c.gridy = 14;
        c.ipadx = 0;
        c.ipady = 0;
        ss.setEnabled(false);
        add(ss, c);

        label = new JLabel("GS");
        c.gridx = 0;
        c.gridy = 15;
        c.ipadx = 20;
        c.ipady = 20;
        add(label, c);
        gs = new JTextField(9);
        c.gridx = 1;
        c.gridy = 15;
        c.ipadx = 0;
        c.ipady = 0;
        gs.setEnabled(false);
        add(gs, c);

        label = new JLabel("SF");
        c.gridx = 0;
        c.gridy = 10;
        c.ipadx = 20;
        c.ipady = 20;
        add(label, c);
        sf = new JTextField(9);
        c.gridx = 1;
        c.gridy = 10;
        c.ipadx = 0;
        c.ipady = 0;
        add(sf, c);

        c.gridx = 0;
        c.gridy = 16;
        c.gridwidth = 2;
        c.ipadx = 20;
        c.ipady = 20;
        applyBut = new JButton("Apply");
        add(applyBut, c);
        applyBut.addActionListener(new ApplyRegisters());
    }

    private class ApplyRegisters implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            RM rm = RM.getInstance();
            InfoBlock.this.saveRegisters();
        }
    }

    public void setEnabled(boolean enable) {
        this.ti.setEnabled(enable);
        this.ip.setEnabled(enable);
        this.sp.setEnabled(enable);
        this.sf.setEnabled(enable);
        this.applyBut.setEnabled(enable);
    }

    public void loadRegisters() {
        RM rm = RM.getInstance();
        Word command = rm.getCurrentVM().getCurrentCommand();
        this.command.setText((command != null)?command.toStringSymbols():"");
        this.si.setText(Byte.toString(rm.getSI()));
        this.ioi.setText(Byte.toString(rm.getIOI()));
        this.ti.setText(Byte.toString(rm.getTI()));
        this.pi.setText(Byte.toString(rm.getPI()));
        this.ip.setText(rm.getCurrentVM().getIP().toString());
        this.sp.setText(rm.getCurrentVM().getSP().toString());
        this.ptr.setText(rm.getPTR().toString());
        this.mode.setText(Byte.toString(rm.getMode()));
        this.cs.setText(rm.getCurrentVM().getCS().toString());
        this.ss.setText(rm.getCurrentVM().getSS().toString());
        this.ds.setText(rm.getCurrentVM().getDS().toString());
        this.gs.setText(rm.getGS().toString());
        this.sf.setText(rm.getCurrentVM().getSF().toString());
        repaint();
    }

    public void saveRegisters() {
        RM rm = RM.getInstance();
        try {
            rm.setTI(Byte.parseByte(this.ti.getText()));
            rm.getCurrentVM().setIP(new Word(this.ip.getText()));
            rm.getCurrentVM().setSP(new Word(this.sp.getText()));
            rm.getCurrentVM().setSF(new Word(this.sf.getText()));
        }
        catch (Exception e) {
            Window.getInstance().showError(e.toString());
        }
    }
}
