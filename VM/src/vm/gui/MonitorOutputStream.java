package vm.gui;

import java.io.OutputStream;
import javax.swing.JTextArea;

public class MonitorOutputStream extends OutputStream {
    private JTextArea textarea;
    public MonitorOutputStream(JTextArea textarea) {
        this.textarea = textarea;
    }

    public void write(int b) {
        textarea.append(Character.toString((char)b));
        textarea.setCaretPosition(textarea.getDocument().getLength());
    }
}
