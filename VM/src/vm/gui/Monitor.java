package vm.gui;

import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.GridBagConstraints;
import java.awt.event.AdjustmentListener;
import java.awt.event.AdjustmentEvent;

public class Monitor extends JPanel {

    private JTextArea textarea;
    private JTextField input;
    public Monitor(LayoutManager l) {
        super(l);
        GridBagConstraints c = new GridBagConstraints();

        c.gridx = 0;
        c.gridy = 0;
        textarea = new JTextArea();
        JScrollPane scrollPane = new JScrollPane(textarea); 
        textarea.setEditable(false);
        textarea.setLineWrap(true);
        scrollPane.setPreferredSize(new Dimension(800, 600));

        add(scrollPane, c);

        input = new JTextField();
        c.gridx = 0;
        c.gridy = 1;
        c.fill = GridBagConstraints.BOTH;
        input.setEnabled(false);
        add(input, c);

    }

    public JTextArea getTextArea() {
        return this.textarea;
    }

    public JTextField getTextField() {
        return this.input;
    }

}
