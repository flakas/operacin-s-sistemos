package vm.gui;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import java.awt.Container;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagLayout;


public class Window extends JFrame {
    private static Window instance;
    private MainArea mainArea;

    private RealMemoryWindow realMemWin = new RealMemoryWindow();
    private VirtualMemoryWindow virtMemWin = new VirtualMemoryWindow();

    private Window() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Virtual machine");
        setSize(1024, 768);

        GridBagLayout gridbag = new GridBagLayout();
        mainArea = new MainArea(gridbag);

        mainArea.setPreferredSize(new Dimension(1024, 768));

        Container contentPane = getContentPane();

        contentPane.add(mainArea, BorderLayout.CENTER);


        pack();
    }

    public static Window getInstance() {
        return (instance == null) ? instance = new Window() : instance;
    }

    public void makeVisible() {
        setVisible(true);
    }

    public void setEnabled(boolean enable) {
        this.mainArea.setEnabled(enable);
    }

    public MainArea getMainArea() {
        return (MainArea)this.mainArea;
    }

    public void loadRegisters() {
        this.mainArea.loadRegisters();
    }

    public void loadMemory() {
        this.realMemWin.update();
        this.virtMemWin.update();
    }

    public RealMemoryWindow getRealMemoryWindow() {
        return this.realMemWin;
    }

    public VirtualMemoryWindow getVirtualMemoryWindow() {
        return this.virtMemWin;
    }

    public void setEnabledInput(boolean enable) {
        this.mainArea.setEnabledInput(enable);
    }

    public void showError(String text) {
        JOptionPane.showMessageDialog(this, text, "Error", JOptionPane.ERROR_MESSAGE);
    }
}
