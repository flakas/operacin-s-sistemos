package vm.gui;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.LayoutManager;

import java.io.PrintStream;

import vm.*;

public class MainArea extends JPanel {
    private Monitor monitor;
    private InfoBlock infoBlock;
    private Controls controls;

    public MainArea(LayoutManager l) {
        super(l);
        GridBagConstraints c = new GridBagConstraints();

        monitor = new Monitor(l);
        c.gridx = 0;
        c.gridy = 0;
        c.fill = GridBagConstraints.BOTH;
        add(monitor, c);

        infoBlock = new InfoBlock(l);
        c.gridx = 1;
        c.gridy = 0;
        add(infoBlock, c);

        controls = new Controls(l);
        c.gridx = 0;
        c.gridy = 1;
        add(controls, c);

        System.setOut(new PrintStream(new MonitorOutputStream(((Monitor)monitor).getTextArea())));
        System.setIn(new MonitorInputStream(((Monitor)monitor).getTextField()));
    }

    public void setEnabled(boolean enabled) {
        this.infoBlock.setEnabled(enabled);
        this.controls.setEnabled(enabled);
    }

    public void loadRegisters() {
        this.infoBlock.loadRegisters();
    }

    public void setEnabledInput(boolean enable) {
        this.monitor.getTextField().setEnabled(enable);
    }
}
