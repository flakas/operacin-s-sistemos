package vm.gui;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Container;

import vm.*;

public class VirtualMemoryWindow extends JFrame {
    private JTextArea textarea;

    public VirtualMemoryWindow() {
        setTitle("Virtual memory");
        setSize(800, 600);


        JPanel content = new JPanel();

        Container contentPane = getContentPane();

        contentPane.add(content, BorderLayout.CENTER);

        textarea = new JTextArea();
        JScrollPane scrollPane = new JScrollPane(textarea); 
        textarea.setEditable(false);
        textarea.setLineWrap(true);
        scrollPane.setPreferredSize(new Dimension(800, 600));
        content.add(scrollPane);


        pack();

        update();

    }

    public void update() {
        RM rm = RM.getInstance();
        if (rm.getCurrentVM() != null) {
            this.textarea.setText(rm.getCurrentVM().toMemoryString());
        }
        repaint();
    }

    public void makeVisible() {
        setVisible(true);
    }
}


