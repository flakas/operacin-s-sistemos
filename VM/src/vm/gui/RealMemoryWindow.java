package vm.gui;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Container;

import vm.*;

public class RealMemoryWindow extends JFrame {
    private JTextArea textarea;

    public RealMemoryWindow() {
        setTitle("Real memory");
        setSize(800, 600);

        JPanel content = new JPanel();

        Container contentPane = getContentPane();

        contentPane.add(content, BorderLayout.CENTER);

        textarea = new JTextArea();
        JScrollPane scrollPane = new JScrollPane(textarea); 
        textarea.setEditable(false);
        textarea.setLineWrap(true);
        scrollPane.setPreferredSize(new Dimension(800, 600));
        content.add(scrollPane);


        pack();

        update();

    }

    public void update() {
        RM rm = RM.getInstance();
        this.textarea.setText(rm.getMemory().toString());
        repaint();
    }

    public void makeVisible() {
        setVisible(true);
    }
}


