package vm.gui;

import java.io.InputStream;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MonitorInputStream extends InputStream implements ActionListener {
    private JTextField textfield;
    private String str;
    private int pos;

    public MonitorInputStream(JTextField textfield) {
        this.textfield = textfield;
        this.textfield.addActionListener(this);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        Window.getInstance().setEnabled(true);
        Window.getInstance().setEnabledInput(false);
        this.str = this.textfield.getText() + "\n";
        pos = 0;
        this.textfield.setText("");
        synchronized (this) {
            this.notify();
        }
    }

    @Override
    public int read() {
        if (this.str == null || this.pos == this.str.length()) {
            Window.getInstance().setEnabled(false);
            Window.getInstance().setEnabledInput(true);
        }
        while (this.str == null || this.pos >= this.str.length()) {
            try {
                synchronized (this) {
                    this.wait();
                }
            } catch (InterruptedException e) {
            }
        }
        System.out.print(this.str.charAt(this.pos));

        return this.str.charAt(this.pos++);
    }
}
