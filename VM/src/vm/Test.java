import java.util.Arrays;
import java.io.File;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.ByteArrayOutputStream;

import vm.*;

public class Test {
    private static boolean printPassed = false;

    public static void main(String[] args) {
        try
        {
            RM.getInstance().createVM();
            testAdd();
            testSub();
            testMul();
            testAdd();
            testWordEquals();
            testWordCompare();
            testDiv();
            testMod();
            testDec();
            testInc();
            testXor();
            testAnd();
            testOr();
            testNot();
            testCmp();
            testJump();
            testJE();
            testJL();
            testJg();
            testJc();
            testJZ();
            testJNZ();
            testMemoryBlocks();
            testCreateVM();
            testMemoryIndex();
            testMemoryAllocation();
            testExternalMemory();
            testCD();
            testGD();
            testIG();
            testOG();
            testGetRealBlockAddress();
            testSetVMWord();
            testGetVMWord();
            testLoadProgramToVM();
            testInternalAdd();
            testInternalSub();
            testInputChannel();
            testStack();
            testStep();
            testOutputChannel();

            //System.out.println(RM.getInstance().getMemory());
        }
        catch (InterruptException e) {
            e.printStackTrace();
        }
    }

    public static boolean testAdd() {
        RM rm = RM.getInstance();

        // Simple add
        Word w1 = new Word(new short[] {1, 1, 1, 1});
        Word w2 = new Word(new short[] {1, 1, 1, 1});
        if (!compareWords("ADD1 error", rm.getCurrentVM().add(w1, w2), new Word(new short[] {2, 2, 2, 2}))) {
            return false;
        }

        // Add with carry
        w1 = new Word(new short[] {1, 1, 1, 255});
        w2 = new Word(new short[] {1, 1, 1, 1});
        if (!compareWords("ADD2 error", rm.getCurrentVM().add(w1, w2), new Word(new short[] {2, 2, 3, 0}))) {
            return false;
        }
        if (rm.getCurrentVM().getSF().getByte(2) != (short)1) {
            System.out.println("ADD2 error: Expected CF=1, got CF=0");
            return false;
        }

        // Add with overflow
        w1 = new Word(new short[] {255, 255, 255, 255});
        w2 = new Word(new short[] {1, 1, 1, 1});
        if (!compareWords("ADD3 error", rm.getCurrentVM().add(w1, w2), new Word(new short[] {1, 1, 1, 0}))) {
            return false;
        }
        if (rm.getCurrentVM().getSF().getByte(2) != (short)1) {
            System.out.println("ADD3 error: Expected CF=1, got CF=0");
            return false;
        }
        if (rm.getCurrentVM().getSF().getByte(0) != (short)1) {
            System.out.println("ADD3 error: Expected OF=1, got OF=0");
            return false;
        }

        printPassed("ADD passed");
        return true;
    }

    private static boolean testSub() {
        RM rm = RM.getInstance();

        // Simple sub
        Word w1 = new Word(new short[] {2, 1, 1, 1});
        Word w2 = new Word(new short[] {1, 1, 1, 1});

        if (!compareWords("SUB1 error", rm.getCurrentVM().sub(w1, w2), new Word(new short[] {1, 0, 0, 0}))) {
            return false;
        }

        // Sub with CF borrow
        w1 = new Word(new short[] {1, 2, 1, 1});
        w2 = new Word(new short[] {1, 1, 2, 1});
        if (!compareWords("SUB2 error", rm.getCurrentVM().sub(w1, w2), new Word(new short[] {0, 0, 255, 0}))) {
            return false;
        }
        if (rm.getCurrentVM().getSF().getByte(2) != (short)1) {
            System.out.println("SUB2 error: Expected CF=1, got CF=0");
            return false;
        }

        // Sub with OF borrow
        w1 = new Word(new short[] {3, 3, 1, 1});
        w2 = new Word(new short[] {4, 3, 1, 1});

        if (!compareWords("SUB3 error", rm.getCurrentVM().sub(w1, w2), new Word(new short[] {255, 0, 0, 0}))) {
            return false;
        }
        if (rm.getCurrentVM().getSF().getByte(2) != (short)0) {
            System.out.println("SUB3 error: Expected CF=0, got CF=1");
            return false;
        }
        if (rm.getCurrentVM().getSF().getByte(0) != (short)1) {
            System.out.println("SUB3 error: Expected OF=1, got OF=0");
            return false;
        }

        printPassed("SUB passed");
        return true;
    }

    private static boolean testMul() {
        RM rm = RM.getInstance();

        // Simple mul
        Word w1 = new Word(new short[] {0, 0, 0, 2});
        Word w2 = new Word(new short[] {0, 0, 0, 7});

        if (!compareWords("MUL1 error", rm.getCurrentVM().mul(w1, w2), new Word(new short[] {0, 0, 0, 14}))) {
            return false;
        }

        // MUL with CF
        w1 = new Word(new short[] {0, 0, 0, 255});
        w2 = new Word(new short[] {0, 0, 0, 255});

        if (!compareWords("MUL2 error", rm.getCurrentVM().mul(w1, w2), new Word(new short[] {0, 0, 254, 1}))) {
            return false;
        }
        if (rm.getCurrentVM().getSF().getByte(2) != (short)1) {
            System.out.println("MUL2 error: Expected CF=1, got CF=0");
            return false;
        }

        // MUL with OF
        w1 = new Word(new short[] {255, 255, 8, 0});
        w2 = new Word(new short[] {0, 0, 0, 9});

        if (!compareWords("MUL3 error", rm.getCurrentVM().mul(w1, w2), new Word(new short[] {255, 247, 72, 0}))) {
            return false;
        }
        if (rm.getCurrentVM().getSF().getByte(2) != (short)1) {
            System.out.println("MUL3 error: Expected CF=1, got CF=0");
            return false;
        }
        if (rm.getCurrentVM().getSF().getByte(0) != (short)1) {
            System.out.println("MUL3 error: Expected OF=1, got OF=0");
            return false;
        }

        // MUL with zero
        w1 = new Word(new short[] {9, 13, 8, 0});
        w2 = new Word(new short[] {0, 0, 0, 0});

        if (!compareWords("MUL4 error", rm.getCurrentVM().mul(w1, w2), new Word(new short[] {0, 0, 0, 0}))) {
            return false;
        }

        // MUL with zero
        w1 = new Word(new short[] {0, 0, 0, 0});
        w2 = new Word(new short[] {9, 13, 8, 0});

        if (!compareWords("MUL5 error", rm.getCurrentVM().mul(w1, w2), new Word(new short[] {0, 0, 0, 0}))) {
            return false;
        }

        printPassed("MUL passed");
        return true;
    }

    private static boolean testDiv()
    throws InterruptException {
        RM rm = RM.getInstance();

        // Simple div
        Word w1 = new Word(new short[] {0, 0, 0, 6});
        Word w2 = new Word(new short[] {0, 0, 0, 2});

        if (!compareWords("DIV1 error", rm.getCurrentVM().div(w1, w2), new Word(new short[] {0, 0, 0, 3}))) {
            return false;
        }

        // DIV with zero
        w1 = new Word(new short[] {9, 13, 8, 0});
        w2 = new Word(new short[] {0, 0, 0, 0});

        try {
            rm.getCurrentVM().div(w1, w2);
        }
        catch (InterruptException e) {
            if (e.getCode() != (byte)2) {
                System.out.println("DIV2 error: Expected PI=2, got PI=" + rm.getPI());
                return false;
            }
        }

        // DIV with zero
        w1 = new Word(new short[] {0, 0, 0, 0});
        w2 = new Word(new short[] {9, 15, 1, 2});

        if (!compareWords("DIV3 error", rm.getCurrentVM().div(w1, w2), new Word(new short[] {0, 0, 0, 0}))) {
            return false;
        }

        // DIV smaller from larger
        w1 = new Word(new short[] {0, 0, 2, 15});
        w2 = new Word(new short[] {0, 3, 1, 2});

        if (!compareWords("DIV4 error", rm.getCurrentVM().div(w1, w2), new Word(new short[] {0, 0, 0, 0}))) {
            return false;
        }

        // DIV large div
        w1 = new Word(new short[] {15, 9, 12, 15});
        w2 = new Word(new short[] {0, 3, 1, 2});

        if (!compareWords("DIV5 error", rm.getCurrentVM().div(w1, w2), new Word(new short[] {0, 0, 5, 1}))) {
            return false;
        }

        // DIV extra large div
        w1 = new Word(new short[] {255, 255, 255, 255});
        w2 = new Word(new short[] {0, 0, 0, 2});

        if (!compareWords("DIV6 error", rm.getCurrentVM().div(w1, w2), new Word(new short[] {127, 255, 255, 255}))) {
            return false;
        }

        printPassed("DIV passed");
        return true;
    }

    private static boolean testMod()
    throws InterruptException {
        RM rm = RM.getInstance();

        // Simple mod
        Word w1 = new Word(new short[] {0, 0, 0, 15});
        Word w2 = new Word(new short[] {0, 0, 0, 9});

        if (!compareWords("MOD1 error", rm.getCurrentVM().mod(w1, w2), new Word(new short[] {0, 0, 0, 6}))) {
            return false;
        }

        // MOD with zero
        w1 = new Word(new short[] {9, 13, 8, 0});
        w2 = new Word(new short[] {0, 0, 0, 0});

        try {
            rm.getCurrentVM().mod(w1, w2);
        }
        catch (InterruptException e)
        {
            if (e.getCode() != (byte)2) {
                System.out.println("MOD2 error: Expected PI=2, got PI=" + rm.getPI());
                return false;
            }
        }

        // MOD with zero
        w1 = new Word(new short[] {0, 0, 0, 0});
        w2 = new Word(new short[] {9, 15, 1, 2});

        if (!compareWords("MOD3 error", rm.getCurrentVM().mod(w1, w2), new Word(new short[] {0, 0, 0, 0}))) {
            return false;
        }

        // MOD smaller from larger
        w1 = new Word(new short[] {0, 0, 2, 15});
        w2 = new Word(new short[] {0, 3, 1, 2});

        if (!compareWords("MOD4 error", rm.getCurrentVM().mod(w1, w2), new Word(new short[] {0, 0, 2, 15}))) {
            return false;
        }

        // MOD large div
        w1 = new Word(new short[] {15, 9, 12, 15});
        w2 = new Word(new short[] {0, 3, 1, 0});

        if (!compareWords("MOD5 error", rm.getCurrentVM().mod(w1, w2), new Word(new short[] {0, 1, 11, 15}))) {
            return false;
        }

        // MOD extra large div
        w1 = new Word(new short[] {15, 15, 15, 15});
        w2 = new Word(new short[] {0, 0, 0, 2});

        if (!compareWords("MOD6 error", rm.getCurrentVM().mod(w1, w2), new Word(new short[] {0, 0, 0, 1}))) {
            return false;
        }

        printPassed("MOD passed");
        return true;
    }

    private static boolean testWordEquals() {
        Word word1 = new Word(new short[] {1, 1, 1, 1});
        Word word2 = new Word(new short[] {1, 1, 1, 1});

        if (!word1.equals(word2)) {
            System.out.println("testWordEquals1 error: Expected: true. got: " + Boolean.toString(word1.equals(word2)));
            return false;
        }

        word1 = new Word(new short[] {2, 1, 1, 1});
        word2 = new Word(new short[] {1, 1, 15, 1});

        if (word1.equals(word2)) {
            System.out.println("testWordEquals2 error: Expected: false. got: " + Boolean.toString(word1.equals(word2)));
            return false;
        }

        printPassed("WordEquals passed");
        return true;
    }

    private static boolean testWordCompare() {
        Word word1 = new Word(new short[] {1, 1, 1, 1});
        Word word2 = new Word(new short[] {1, 1, 1, 1});

        if (word1.compareTo(word2) != 0) {
            System.out.println("testWordCompare1 error: Expected: 0. got: " + word1.compareTo(word2));
            return false;
        }

        word1 = new Word(new short[] {2, 1, 1, 1});
        word2 = new Word(new short[] {1, 1, 15, 1});

        if (word1.compareTo(word2) != 1) {
            System.out.println("testWordCompare2 error: Expected: 1. got: " + word1.compareTo(word2));
            return false;
        }

        word1 = new Word(new short[] {0, 1, 1, 1});
        word2 = new Word(new short[] {1, 1, 15, 1});

        if (word1.compareTo(word2) != -1) {
            System.out.println("testWordCompare3 error: Expected: -1. got: " + word1.compareTo(word2));
            return false;
        }

        word1 = new Word(new short[] {1, 1, 1, 2});
        word2 = new Word(new short[] {1, 1, 1, 1});

        if (word1.compareTo(word2) != 1) {
            System.out.println("testWordCompare4 error: Expected: 1. got: " + word1.compareTo(word2));
            return false;
        }

        word1 = new Word(new short[] {0, 0, 1, 2});
        word2 = new Word(new short[] {0, 0, 2, 1});

        if (word1.compareTo(word2) != -1) {
            System.out.println("testWordCompare5 error: Expected: -1. got: " + word1.compareTo(word2));
            return false;
        }

        printPassed("testWordCompare passed");
        return true;
    }

    public static boolean testDec() {
        RM rm = RM.getInstance();

        // Simple dec
        Word w1 = new Word(new short[] {1, 1, 1, 1});
        if (!compareWords("DEC1 error", rm.getCurrentVM().dec(w1), new Word(new short[] {1, 1, 1, 0}))) {
            return false;
        }

        // Dec with carry
        w1 = new Word(new short[] {1, 1, 1, 0});
        if (!compareWords("DEC2 error", rm.getCurrentVM().dec(w1), new Word(new short[] {1, 1, 0, 255}))) {
            return false;
        }

        if (rm.getCurrentVM().getSF().getByte(2) != (short)1) {
            System.out.println("DEC2 error: Expected CF=1, got CF=0");
            return false;
        }

        // Dec with overflow
        w1 = new Word(new short[] {0, 0, 0, 0});
        if (!compareWords("DEC3 error", rm.getCurrentVM().dec(w1), new Word(new short[] {255, 255, 255, 255}))) {
            return false;
        }
        if (rm.getCurrentVM().getSF().getByte(2) != (short)1) {
            System.out.println("DEC3 error: Expected CF=1, got CF=0");
            return false;
        }
        if (rm.getCurrentVM().getSF().getByte(0) != (short)1) {
            System.out.println("DEC3 error: Expected OF=1, got OF=0");
            return false;
        }

        printPassed("DEC passed");
        return true;
    }

    public static boolean testInc() {
        RM rm = RM.getInstance();

        // Simple inc
        Word w1 = new Word(new short[] {1, 1, 1, 1});
        if (!compareWords("INC1 error", rm.getCurrentVM().inc(w1), new Word(new short[] {1, 1, 1, 2}))) {
            return false;
        }

        // Inc with carry
        w1 = new Word(new short[] {1, 1, 1, 255});
        if (!compareWords("INC2 error", rm.getCurrentVM().inc(w1), new Word(new short[] {1, 1, 2, 0}))) {
            return false;
        }

        if (rm.getCurrentVM().getSF().getByte(2) != (short)1) {
            System.out.println("INC2 error: Expected CF=1, got CF=0");
            return false;
        }

        // Add with overflow
        w1 = new Word(new short[] {255, 255, 255, 255});
        if (!compareWords("INC3 error", rm.getCurrentVM().inc(w1), new Word(new short[] {0, 0, 0, 0}))) {
            return false;
        }
        if (rm.getCurrentVM().getSF().getByte(2) != (short)1) {
            System.out.println("INC3 error: Expected CF=1, got CF=0");
            return false;
        }
        if (rm.getCurrentVM().getSF().getByte(0) != (short)1) {
            System.out.println("INC3 error: Expected OF=1, got OF=0");
            return false;
        }

        printPassed("INC passed");
        return true;
    }

    private static boolean testXor() {
        RM rm = RM.getInstance();

        // Simple xor
        Word w1 = new Word(new short[] {0, 0, 0, 15});
        Word w2 = new Word(new short[] {0, 0, 0, 9});

        if (!compareWords("XOR1 error", rm.getCurrentVM().xor(w1, w2), new Word(new short[] {0, 0, 0, 6}))) {
            return false;
        }

        // Larger xor
        w1 = new Word(new short[] {15, 9, 2, 8});
        w2 = new Word(new short[] {1, 0, 13, 8});

        if (!compareWords("XOR2 error", rm.getCurrentVM().xor(w1, w2), new Word(new short[] {14, 9, 15, 0}))) {
            return false;
        }

        // Zero and FFFF xor
        w1 = new Word(new short[] {0, 0, 0, 0});
        w2 = new Word(new short[] {15, 15, 15, 15});

        if (!compareWords("XOR3 error", rm.getCurrentVM().xor(w1, w2), new Word(new short[] {15, 15, 15, 15}))) {
            return false;
        }


        printPassed("XOR passed");
        return true;
    }

    private static boolean testAnd() {
        RM rm = RM.getInstance();

        // Simple and
        Word w1 = new Word(new short[] {0, 0, 0, 15});
        Word w2 = new Word(new short[] {0, 0, 0, 9});

        if (!compareWords("AND1 error", rm.getCurrentVM().and(w1, w2), new Word(new short[] {0, 0, 0, 9}))) {
            return false;
        }

        // Larger and
        w1 = new Word(new short[] {15, 9, 2, 8});
        w2 = new Word(new short[] {1, 0, 13, 8});

        if (!compareWords("AND2 error", rm.getCurrentVM().and(w1, w2), new Word(new short[] {1, 0, 0, 8}))) {
            return false;
        }

        // Zero and FFFF and
        w1 = new Word(new short[] {0, 0, 0, 0});
        w2 = new Word(new short[] {15, 15, 15, 15});

        if (!compareWords("AND3 error", rm.getCurrentVM().and(w1, w2), new Word(new short[] {0, 0, 0, 0}))) {
            return false;
        }

        printPassed("AND passed");
        return true;
    }

    private static boolean testOr() {
        RM rm = RM.getInstance();

        // Simple or
        Word w1 = new Word(new short[] {0, 0, 0, 15});
        Word w2 = new Word(new short[] {0, 0, 0, 9});

        if (!compareWords("OR1 error", rm.getCurrentVM().or(w1, w2), new Word(new short[] {0, 0, 0, 15}))) {
            return false;
        }

        // Larger or
        w1 = new Word(new short[] {15, 9, 2, 8});
        w2 = new Word(new short[] {1, 0, 13, 8});

        if (!compareWords("OR2 error", rm.getCurrentVM().or(w1, w2), new Word(new short[] {15, 9, 15, 8}))) {
            return false;
        }

        // Zero and FFFF or
        w1 = new Word(new short[] {0, 0, 0, 0});
        w2 = new Word(new short[] {15, 15, 15, 15});

        if (!compareWords("OR3 error", rm.getCurrentVM().or(w1, w2), new Word(new short[] {15, 15, 15, 15}))) {
            return false;
        }

        printPassed("OR passed");
        return true;
    }

    private static boolean testNot() {
        RM rm = RM.getInstance();

        // Simple not
        Word w = new Word(new short[] {0, 0, 0, 255});

        if (!compareWords("NOT1 error", rm.getCurrentVM().not(w), new Word(new short[] {255, 255, 255, 0}))) {
            return false;
        }

        // Larger not
        w = new Word(new short[] {255, 9, 2, 8});

        if (!compareWords("NOT2 error", rm.getCurrentVM().not(w), new Word(new short[] {0, 246, 253, 247}))) {
            return false;
        }

        // Zero not
        w = new Word(new short[] {0, 0, 0, 0});

        if (!compareWords("NOT3 error", rm.getCurrentVM().not(w), new Word(new short[] {255, 255, 255, 255}))) {
            return false;
        }

        // FFFF not
        w = new Word(new short[] {255, 255, 255, 255});

        if (!compareWords("NOT4 error", rm.getCurrentVM().not(w), new Word(new short[] {0, 0, 0, 0}))) {
            return false;
        }

        printPassed("NOT passed");
        return true;
    }

    private static boolean testCmp() {
        RM rm = RM.getInstance();

        // Equal compare
        Word w1 = new Word(new short[] {1, 1, 1, 1});
        Word w2 = new Word(new short[] {1, 1, 1, 1});

        rm.getCurrentVM().cmp(w1, w2);

        if (rm.getCurrentVM().getSF().getByte(3) != (short)1) {
            System.out.println("Cmp1 error: Expected ZF=1, got ZF=0");
            return false;
        }
        if (rm.getCurrentVM().getSF().getByte(1) != (short)0) {
            System.out.println("Cmp1 error: Expected CmpF=0, got CmpF=" + rm.getCurrentVM().getSF().getByte(1));
            return false;
        }

        // First bigger compare
        w1 = new Word(new short[] {2, 1, 1, 1});
        w2 = new Word(new short[] {1, 1, 15, 1});

        rm.getCurrentVM().cmp(w1, w2);

        if (rm.getCurrentVM().getSF().getByte(3) != (short)0) {
            System.out.println("Cmp2 error: Expected ZF=0, got ZF=1");
            return false;
        }
        if (rm.getCurrentVM().getSF().getByte(1) != (short)2) {
            System.out.println("Cmp2 error: Expected CmpF=2, got CmpF=" + rm.getCurrentVM().getSF().getByte(1));
            return false;
        }

        // Second bigger compare
        w1 = new Word(new short[] {0, 1, 1, 1});
        w2 = new Word(new short[] {1, 1, 15, 1});

        rm.getCurrentVM().cmp(w1, w2);

        if (rm.getCurrentVM().getSF().getByte(3) != (short)0) {
            System.out.println("Cmp3 error: Expected ZF=0, got ZF=1");
            return false;
        }
        if (rm.getCurrentVM().getSF().getByte(1) != (short)1) {
            System.out.println("Cmp3 error: Expected CmpF=1, got CmpF=" + rm.getCurrentVM().getSF().getByte(1));
            return false;
        }

        // Zero compare
        w1 = new Word(new short[] {0, 0, 0, 0});
        w2 = new Word(new short[] {0, 0, 0, 0});

        rm.getCurrentVM().cmp(w1, w2);

        if (rm.getCurrentVM().getSF().getByte(3) != (short)1) {
            System.out.println("Cmp4 error: Expected ZF=1, got ZF=0");
            return false;
        }
        if (rm.getCurrentVM().getSF().getByte(1) != (short)0) {
            System.out.println("Cmp4 error: Expected CmpF=0, got CmpF=" + rm.getCurrentVM().getSF().getByte(1));
            return false;
        }

        printPassed("CMP passed");
        return true;
    }

    private static boolean testJump() {
       RM rm = RM.getInstance();

        // Simple Compare
        Word w = new Word(new short[] {0, 0, 0, 51});

        try {
            rm.getCurrentVM().jump(w);
            if (!compareWords("JUMP1 error", rm.getCurrentVM().getIP(), new Word(new short[] {0, 0, 0, 51}))) {
                return false;
            }
        } catch (InterruptException ex) {
            System.out.println("JUMP1 error - InterruptException thrown");
            return false;
        }

        w = new Word(new short[] {0, 0, 1, 10});
        try {
            rm.getCurrentVM().jump(w);
            if (!compareWords("JUMP2 error", rm.getCurrentVM().getIP(), new Word(new short[] {0, 0, 1, 10}))) {
                return false;
            }
        } catch (InterruptException ex) {
            System.out.println("JUMP2 error - InterruptException thrown");
            return false;
        }

        w = new Word(new short[] {15, 15, 15, 15});
        try {
            rm.getCurrentVM().jump(w);
            System.out.println("JUMP3 error - InterruptException not thrown");
            return false;
        } catch (InterruptException ex) {
            if (ex.getCode() != 3) {
                System.out.println("JUMP3 error - 3 expected, got " + ex.getCode());
                return false;
            }
            // OK
        }



        printPassed("JUMP passed");
        return true;
    }

    private static boolean testJE() {
        RM rm = RM.getInstance();

        rm.getCurrentVM().cmp(new Word(new short[] {1, 1, 1, 3}), new Word(new short[] {1, 1, 1, 3}));

        Word w = new Word(new short[] {0, 0, 0, 2});

        try {
            if (rm.getCurrentVM().je(w) && !compareWords("JE1 error", rm.getCurrentVM().getIP(), new Word(new short[] {0, 0, 0, 2}))) {
                return false;
            }

        } catch (InterruptException ex) {
            System.out.println("JE1 error - InterruptException thrown");
            return false;
        }

        rm.getCurrentVM().cmp(new Word(new short[] {1, 1, 1, 5}), new Word(new short[] {1, 1, 1, 3}));

        w = new Word(new short[] {0, 0, 0, 15});

        try {
            if (!rm.getCurrentVM().je(w) && !compareWords("JE2 error", rm.getCurrentVM().getIP(), new Word(new short[] {0, 0, 0, 2}))) {
                return false;
            }
        } catch (InterruptException ex) {
            System.out.println("JE2 error - InterruptException thrown");
            return false;
        }

        printPassed("JE passed");
        return true;
    }

    private static boolean testJL() {
        RM rm = RM.getInstance();

        rm.getCurrentVM().cmp(new Word(new short[] {1, 1, 1, 3}), new Word(new short[] {1, 1, 1, 5}));

        Word w = new Word(new short[] {0, 0, 0, 2});

        try {
            if (rm.getCurrentVM().jl(w) && !compareWords("JL1 error", rm.getCurrentVM().getIP(), new Word(new short[] {0, 0, 0, 2}))) {
                return false;
            }
        } catch (InterruptException ex) {
            System.out.println("JL1 error - InterruptException thrown");
            return false;
        }

        rm.getCurrentVM().cmp(new Word(new short[] {1, 1, 1, 8}), new Word(new short[] {1, 1, 1, 5}));

        w = new Word(new short[] {0, 0, 0, 55});

        try {
            if (!rm.getCurrentVM().jl(w) && !compareWords("JL2 error", rm.getCurrentVM().getIP(), new Word(new short[] {0, 0, 0, 2}))) {
                return false;
            }
        } catch (InterruptException ex) {
            System.out.println("JL2 error - InterruptException thrown");
            return false;
        }

        printPassed("JL passed");
        return true;
    }

    private static boolean testJZ() {
        RM rm = RM.getInstance();

        rm.getCurrentVM().cmp(new Word(new short[] {1, 1, 1, 3}), new Word(new short[] {1, 1, 1, 3}));

        Word w = new Word(new short[] {0, 0, 0, 2});

        try {
            if (rm.getCurrentVM().jz(w) && !compareWords("JZ1 error", rm.getCurrentVM().getIP(), new Word(new short[] {0, 0, 0, 2}))) {
                return false;
            }
        } catch (InterruptException ex) {
            System.out.println("JZ1 error - InterruptException thrown");
            return false;
        }

        rm.getCurrentVM().cmp(new Word(new short[] {1, 1, 1, 8}), new Word(new short[] {1, 1, 1, 5}));

        w = new Word(new short[] {0, 0, 0, 55});

        try {
            if (!rm.getCurrentVM().jz(w) && !compareWords("JZ2 error", rm.getCurrentVM().getIP(), new Word(new short[] {0, 0, 0, 2}))) {
                return false;
            }
        } catch (InterruptException ex) {
            System.out.println("JZ2 error - InterruptException thrown");
            return false;
        }

        printPassed("JZ passed");
        return true;
    }

    private static boolean testJNZ() {
        RM rm = RM.getInstance();

        rm.getCurrentVM().cmp(new Word(new short[] {1, 1, 1, 4}), new Word(new short[] {1, 1, 1, 3}));

        Word w = new Word(new short[] {0, 0, 0, 2});

        try {
            if (rm.getCurrentVM().jnz(w) && !compareWords("JNZ1 error", rm.getCurrentVM().getIP(), new Word(new short[] {0, 0, 0, 2}))) {
                return false;
            }
        } catch (InterruptException ex) {
            System.out.println("JNZ1 error - InterruptException thrown");
            return false;
        }

        rm.getCurrentVM().cmp(new Word(new short[] {1, 1, 1, 5}), new Word(new short[] {1, 1, 1, 5}));

        w = new Word(new short[] {0, 0, 0, 55});

        try {
            if (!rm.getCurrentVM().jnz(w) && !compareWords("JNZ2 error", rm.getCurrentVM().getIP(), new Word(new short[] {0, 0, 0, 2}))) {
                return false;
            }
        } catch (InterruptException ex) {
            System.out.println("JNZ2 error - InterruptException thrown");
            return false;
        }


        printPassed("JNZ passed");
        return true;
    }

    private static boolean testJg() {
        RM rm = RM.getInstance();

        // Setting IP
        Word w1 = new Word(new short[] {0, 0, 0, 5});

        try {
            rm.getCurrentVM().jump(w1);
        } catch (InterruptException ex) {
            System.out.println("JG1 error - InterruptException thrown");
            return false;
        }

        if (!compareWords("JG1 error", rm.getCurrentVM().getIP(), new Word(new short[] {0, 0, 0, 5}))) {
            return false;
        }

        // Comparing and changing SF[1]=2
        w1 = new Word(new short[] {15, 15, 15, 15});
        Word w2 = new Word(new short[] {1, 15, 15, 15});
        Word w3 = new Word(new short[] {0, 0, 0, 50});

        rm.getCurrentVM().cmp(w1, w2);

        try {
            rm.getCurrentVM().jg(w3);
        } catch (InterruptException ex) {
            System.out.println("JG2 error - InterruptException thrown");
            return false;
        }

        if (!compareWords("JG2 error", rm.getCurrentVM().getIP(), new Word(new short[] {0, 0, 0, 50}))) {
            return false;
        }

        // Comparing and changing SF[1]=1
        w1 = new Word(new short[] {1, 1, 15, 15});
        w2 = new Word(new short[] {15, 15, 15, 15});

        rm.getCurrentVM().cmp(w1, w2);

        try {
            rm.getCurrentVM().jg(w1);
        } catch (InterruptException ex) {
            System.out.println("JG3 error - InterruptException thrown");
            return false;
        }


        if (!compareWords("JG3 error", rm.getCurrentVM().getIP(), new Word(new short[] {0, 0, 0, 50}))) {
            return false;
        }

        printPassed("JG passed");
        return true;
    }

    private static boolean testJc() {
        RM rm = RM.getInstance();

        // Setting IP
        Word w1 = new Word(new short[] {0, 0, 0, 3});

        try {
            rm.getCurrentVM().jump(w1);
        } catch (InterruptException ex) {
            System.out.println("JC1 error - InterruptException thrown");
            return false;
        }

        if (!compareWords("JC1 error", rm.getCurrentVM().getIP(), new Word(new short[] {0, 0, 0, 3}))) {
            return false;
        }


        // ADDing and changing SF[2]=1
        w1 = new Word(new short[] {0, 0, 0, 255});
        Word w2 = new Word(new short[] {0, 0, 0, 1});

        rm.getCurrentVM().add(w1, w2);

        if (rm.getCurrentVM().getSF().getByte(2) != (short)1) {
            System.out.println("JC2 error: Expected CF=1, got CF=0");
            return false;
        }

        try {
            rm.getCurrentVM().jc(new Word(new short[] {0, 0, 0, 3}));
        } catch (InterruptException ex) {
            System.out.println("JC2 error - InterruptException thrown");
            return false;
        }

        if (!compareWords("JC2 error", rm.getCurrentVM().getIP(), new Word(new short[] {0, 0, 0, 3}))) {
            return false;
        }

        // ADDing and changing SF[2]=0
        w1 = new Word(new short[] {1, 1, 1, 1});
        w2 = new Word(new short[] {1, 1, 1, 1});

        rm.getCurrentVM().add(w1, w2);

        if (rm.getCurrentVM().getSF().getByte(2) != (short)0) {
            System.out.println("JC3 error: Expected CF=0, got CF=1");
            return false;
        }

        try {
            rm.getCurrentVM().jc(w1);
        } catch (InterruptException ex) {
            System.out.println("JC3 error - InterruptException thrown");
            return false;
        }

        if (!compareWords("JC3 error", rm.getCurrentVM().getIP(), new Word(new short[] {0, 0, 0, 3}))) {
            return false;
        }

        printPassed("JC passed");
        return true;
    }

    private static boolean testMemoryBlocks()
    throws InterruptException {
        Memory m = new Memory();

        if (m.isBlockFree(5) != true) {
            System.out.println("Memory Blocks1 error: Expected free block 5, got taken block 5");
            return true;
        }

        Word block = m.getBlock();

        if (m.isBlockFree(block) != false) {
            System.out.println("Memory blocks2 error: Expected taken block " + Arrays.toString(block.toByteArray()) + ", got free block " + Arrays.toString(block.toByteArray()));
            return true;
        }

        if (m.freeBlock(block) != true) {
            System.out.println("Memory blocks3 error: Expected true, got false");
            return true;
        }

        if (m.isBlockFree(block) != true) {
            System.out.println("Memory blocks4 error: Expected free block " + Arrays.toString(block.toByteArray()) + ", got taken block " + Arrays.toString(block.toByteArray()));
            return true;
        }

        printPassed("testMemoryBlocks passed");
        return true;
    }

    private static boolean testCreateVM() {
        RM rm = RM.getInstance();

        if (rm.createVM() != true) {
            System.out.println("CreateVM1 error: Expected true, got false");
        }

        printPassed("testCreateVM passed");
        return true;
    }

    private static boolean testMemoryIndex()
    throws InterruptException {
        Memory m = new Memory();

        // Get block index from address
        Word w = new Word(new short[]{0, 0, 0, 0});

        if (m.blockIndexFromAddress(w) != 0) {
            System.out.println("MemoryIndex1 error: Expected 0, got " + m.blockIndexFromAddress(w));
            return false;
        }

        if (!compareWords("MemoryIndex2 error", m.addressFromBlockIndex(0), new Word(new short[] {0, 0, 0, 0}))) {
            return false;
        }

        // Get block index from address
        w = new Word(new short[]{0, 0, 1, 1});

        if (m.blockIndexFromAddress(w) != 16) {
            System.out.println("MemoryIndex3 error: Expected 16, got " + m.blockIndexFromAddress(w));
            return false;
        }

        if (!compareWords("MemoryIndex4 error", m.addressFromBlockIndex(16), new Word(new short[] {0, 0, 1, 0}))) {
            return false;
        }
        // Get block index from address
        w = new Word(new short[]{0, 0, 0, 255});

        if (m.blockIndexFromAddress(w) != 15) {
            System.out.println("MemoryIndex5 error: Expected 15, got " + m.blockIndexFromAddress(w));
            return false;
        }

        if (!compareWords("MemoryIndex6 error", m.addressFromBlockIndex(15), new Word(new short[] {0, 0, 0, 240}))) {
            return false;
        }

        printPassed("testMemoryIndex passed");
        return true;
    }

    private static boolean testMemoryAllocation()
    throws InterruptException {
        Memory mem = new Memory();

        // Test to write word in not allocated space
        Word w = new Word(new short[]{0, 0, 3, 4});
        mem.freeBlock(new Word(new short[]{0, 0, 0, 85}));
        if (mem.setWord(new Word(new short[]{0, 0, 0, 85}), w) != false) {
            System.out.println("MemoryAllocation1 error: Expected false, got true");
            return false;
        }

        // Testing what was written in not allocated space
        try {
            // Should throw InterruptException 
            if (!compareWords("MemoryAllocation2 error", mem.getWord(new Word(new short[] {0, 0, 0, 85})), new Word(new short[]{0, 0, 0, 0}))) {
                return false;
            }
            System.out.println("MemoryAllocation2 error - Did not throw InterruptException");
            return false;
        } catch (InterruptException ex) {
            // OK
        }

        // Test to write word in allocated space
        Word address = mem.getBlock(21);

        if (address == null || mem.setWordReal(address, w) != true) {
            System.out.println("MemoryAllocation3 error: Expected true, got false");
            return false;
        }

        // Testing what was written in allocated space
        if (!compareWords("MemoryAllocation4 error", mem.getWordReal(address), w)) {
            return false;
        }

        printPassed("testMemoryAllocation passed");
        return true;
    }

    private static boolean testExternalMemory() {
        ExternalMemory em = new ExternalMemory(new File("harddrivetest.txt"));

        Word w = new Word(new short[] {0, 0, 0, 0xFE});
        if (em.blockIndexFromAddress(w) != 15) {
            System.out.println("ExternalMemory1 error: expected 15, got " + em.blockIndexFromAddress(w));
            return false;
        }

        if (em.wordIndexFromAddress(w) != 0xFE) {
            System.out.println("ExternalMemory2 error: expected 254, got " + em.wordIndexFromAddress(w));
            return false;
        }

        w = new Word(new short[] {0, 0, 0, 75});
        if (em.setWord(w, w) != true) {
            System.out.println("ExternalMemory3 error: expected true, got false");
            return false;
        }

        Word w2 = new Word(new short[] {0, 0, 0, 75});
        if (em.getWord(w).equals(w2) != true) {
            System.out.println("ExternalMemory4 error: expected true, got false");
            return false;
        }

        w = new Word(new short[] {0, 0, 0, 2});
        w2 = new Word(new short[] {'P', 'i', 'r', 'm'});

        if (w2.equals(em.getWord(w)) != true) {
            compareWords("ExternalMemory5 error", em.getWord(w), w2);
            return false;
        }

        return true;
    }

    private static boolean testCD()
    throws InterruptException {
        RM rm = RM.getInstance();

        if (rm.getCurrentVM().cd(new Word(new short[]{0, 0, 0, 250}), new Word(new short[]{1, 2, 3, 4})) != true) {
            System.out.println("CD1 error: Expected true, got false");
            return false;
        }

        printPassed("CD passed");
        return true;
    }

    private static boolean testGD()
    throws InterruptException {
        RM rm = RM.getInstance();

        if (!compareWords("GD1 error", rm.getCurrentVM().gd(new Word(new short[]{0, 0, 0, 250})), new Word(new short[]{1, 2, 3, 4}))) {
            return false;
        }

        printPassed("GD passed");
        return true;
    }

    private static boolean testIG()
    throws InterruptException {
        RM rm = RM.getInstance();

        try { // Deny IG if VM does not have the semaphore
            rm.getCurrentVM().ig(new Word(new short[]{0, 0, 5, 5}));
            System.out.println("IG1 error: expected InterruptException, got something");
        } catch (InterruptException ex) {
        }
        rm.getCurrentVM().acq(new Word(new short[] {0, 0, 0, 2})); // Acquire the semaphore first
        rm.getCurrentVM().ig(new Word(new short[]{0, 0, 5, 5}));

        if (rm.getSI() != 6) {
            System.out.println("IG2 error: Expected 6, got " + rm.getSI());
            return false;
        }

        if (rm.getCurrentVM().getCurrentParam(1).equals(new Word(new short[] {0, 0, 5, 5})) != true) {
            System.out.println("IG3 error: Expected true, got false");
            return false;
        }

        printPassed("IG passed");
        return true;
    }

    private static boolean testOG()
    throws InterruptException {
        RM rm = RM.getInstance();

        rm.getCurrentVM().og(new Word(new short[] {0, 0, 5, 5}), new Word(new short[] {1, 2, 3, 4}));

        if (rm.getSI() != 7) {
            System.out.println("OG1 error: Expected 7, got " + rm.getSI());
            return false;
        }

        if (rm.getCurrentVM().getCurrentParam(1).equals(new Word(new short[] {0, 0, 5, 5})) != true) {
            System.out.println("OG2 error: Expected true, got false");
            return false;
        }

        if (rm.getCurrentVM().getCurrentParam(2).equals(new Word(new short[] {1, 2, 3, 4})) != true) {
            System.out.println("OG3 error: Expected true, got false");
            return false;
        }

        printPassed("OG passed");
        return true;
    }

    private static boolean testGetRealBlockAddress() 
    throws InterruptException {
        RM rm = RM.getInstance();

        if (!compareWords("GetRealBlockAddress1 error", rm.getMemory().getRealBlockAddress(new Word(new short[]{0, 0, 0, 32})), new Word(new short[]{0, 0, 3, 192}))) { // Block number 60
            return false;
        }

        printPassed("testGetRealBlockAddress passed");
        return true;
    }

    private static boolean testSetVMWord()
    throws InterruptException {
        RM rm = RM.getInstance();

        if (rm.getCurrentVM().setVMWord((short)1, new Word(new short[]{0, 0, 0, 15}), new Word(new short[]{1, 2, 3, 4})) != true) {
            System.out.println("SetVMWord1 error: Expected true, got false");
            //System.out.println(rm.getMemory());
            System.exit(1);
            return false;
        }

        printPassed("testSetVMWord passed");
        return true;
    }

    private static boolean testGetVMWord()
    throws InterruptException {
        RM rm = RM.getInstance();

        if (!compareWords("GetVMWord1 error", rm.getCurrentVM().getVMWord((short)1, new Word(new short[]{0, 0, 0, 15})), new Word(new short[]{1, 2, 3, 4}))) {
            return false;
        }

        printPassed("testGetVMWord passed");
        return true;
    }

    private static boolean testLoadProgramToVM()
    throws InterruptException {
        RM.clearInstance();
        RM rm = RM.getInstance(new File("harddrivetest.txt"));
        rm.createVM();
        rm.loadProgramToVM(new Word(new short[] {0, 0, 0, 0}));

        if (!compareWords("testLoadProgramToVM1 error", rm.getCurrentVM().getVMWord((short)1, new Word(new short[] {0, 0, 0, 0})), new Word(new short[] {'P', 'i', 'r', 'm'}))) {
            return false;
        }

        if (!compareWords("testLoadProgramToVM2 error", rm.getCurrentVM().getVMWord((short)0, new Word(new short[] {0, 0, 0, 1})), new Word(new short[] {'H', 'A', 'L', 'T'}))) {
            return false;
        }

        printPassed("testLoadProgramToVM passed");
        return true;
    }

    private static boolean testInternalAdd() {
        RM rm = RM.getInstance();

        Word w1 = new Word(new short[]{0, 0, 0, 15});
        Word w2 = new Word(new short[]{0, 0, 0, 1});

        if (!compareWords("testInternalAdd1 error", RM.internalAdd(w1, w2), new Word(new short[] {0, 0, 0, 16}))) {
            return false;
        }

        w1 = new Word(new short[]{0, 0, 15, 0});
        w2 = new Word(new short[]{0, 0, 1, 0});

        if (!compareWords("testInternalAdd2 error", RM.internalAdd(w1, w2), new Word(new short[] {0, 0, 16, 0}))) {
            return false;
        }

        w1 = new Word(new short[]{0, 0, 255, 0});
        w2 = new Word(new short[]{0, 0, 1, 0});

        if (!compareWords("testInternalAdd3 error", RM.internalAdd(w1, w2), new Word(new short[] {0, 1, 0, 0}))) {
            return false;
        }


        printPassed("testInternalAdd passed");
        return true;
    }

    private static boolean testInternalSub() {
        RM rm = RM.getInstance();

        Word w1 = new Word(new short[]{0, 0, 1, 1});
        Word w2 = new Word(new short[]{0, 0, 0, 7});

        if (!compareWords("testInternalSub1 error", RM.internalSub(w1, w2), new Word(new short[] {0, 0, 0, 250}))) {
            return false;
        }

        w1 = new Word(new short[]{0, 1, 1, 0});
        w2 = new Word(new short[]{0, 0, 7, 0});

        if (!compareWords("testInternalSub2 error", RM.internalSub(w1, w2), new Word(new short[] {0, 0, 250, 0}))) {
            return false;
        }

        w1 = new Word(new short[]{0, 0, 0, 0});
        w2 = new Word(new short[]{0, 0, 1, 0});

        if (!compareWords("testInternalSub3 error", RM.internalSub(w1, w2), new Word(new short[] {255, 255, 255, 0}))) {
            return false;
        }


        printPassed("testInternalSub passed");
        return true;
    }

    private static boolean testStack()
    throws InterruptException {
        RM.clearInstance();
        RM rm = RM.getInstance(new File("harddrivetest.txt"));
        rm.createVM();
        VM vm = rm.getCurrentVM();
        Word startingSP = vm.getSP().copy();
        vm.pushStack(new Word(new short[] {1, 2, 3, 4}));

        if (!compareWords("testStack1 error", vm.getSP(), RM.internalSub(startingSP, Word.ONE))) {
            return false;
        }

        if (!compareWords("testStack2 error", vm.peekStack(), new Word(new short[] {1, 2, 3, 4}))) {
            return false;
        }

        startingSP = vm.getSP().copy();

        if (!compareWords("testStack3 error", vm.popStack(), new Word(new short[] {1, 2, 3, 4}))) {
            return false;
        }

        if (!compareWords("testStack4 error", vm.getSP(), RM.internalAdd(startingSP, Word.ONE))) {
            return false;
        }

        printPassed("testStack passed");
        return true;
    }

    private static boolean testStep()
    throws InterruptException {
        RM.clearInstance();
        RM rm = RM.getInstance(new File("harddrivetest.txt"));
        rm.createVM();

        rm.loadProgramToVM(new Word(new short[] {0, 0, 0, 45}));


        VM vm = rm.getCurrentVM();
        Word startingSP = vm.getSP().copy();

        vm.step();

        if (!compareWords("testStep1 error", vm.getSP(), RM.internalSub(startingSP, Word.ONE))) {
            return false;
        }

        if (!compareWords("testStep2 error", vm.peekStack(), new Word(new short[] {0, 0, 0, 1}))) {
            return false;
        }


        vm.step();

        if (!compareWords("testStep2 error", vm.peekStack(), new Word(new short[] {0, 0, 0, 3}))) {
            return false;
        }

        vm.step();

        if (!compareWords("testStep3 error", vm.peekStack(), new Word(new short[] {0, 0, 0, 4}))) {
            return false;
        }

        printPassed("testStep passed");
        return true;
    }


    private static boolean testInputChannel()
    throws InterruptException {
        String data = "Hello, World!\r\n";
        InputStream stdin = System.in;
        InChannel in = new InChannel();
        RM rm = RM.getInstance();
        rm.getCurrentVM().setCurrentParam(1, new Word(new short[] {0, 0, 0, 0}));
        try {
            System.setIn(new ByteArrayInputStream(data.getBytes()));
            in.exchange();
            Word word = rm.getCurrentVM().getVMWord((short)1, new Word(new short[]{0, 0, 0, 0}));
            if (!word.equals(new Word(new short[]{'H', 'e', 'l', 'l'}))) {
                System.out.println("testInputChannel error: expected " + new Word(new short[]{'H', 'e', 'l', 'l'}) + ", got " + word);
                return false;
            }
        } finally {
            System.setIn(stdin);
        }
        return true;
    }

    private static boolean testOutputChannel()
    throws InterruptException {
        String data = new String(new byte[]{'H','e','l','l','o','!',0});
        InputStream stdin = System.in;
        InChannel in = new InChannel();
        RM rm = RM.getInstance();
        rm.getCurrentVM().setCurrentParam(1, new Word(new short[]{0, 0, 0, 0}));
        try {
            System.setIn(new ByteArrayInputStream(data.getBytes()));
            in.exchange();
        }
        finally {
            System.setIn(stdin);
        }

        PrintStream stdout = System.out;
        OutChannel out = new OutChannel();
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        rm.getCurrentVM().setCurrentParam(1, new Word(new short[]{0, 0, 0, 0}));
        try {
            System.setOut(new PrintStream(outContent));
            out.exchange();
        } finally {
            System.setOut(stdout);
        }
        if (!data.equals(outContent.toString())) {
            System.out.println("testOutputChannel error: expected " + data +  ", got " + outContent.toString());
            return false;
        }
        return true;

    }

    private static boolean compareWords(String errorText, Word result, Word expected) {
        for (int i = 0; i < 4; i += 1) {
            if (result == null) {
                System.out.println(result);
            }
            if (!result.equals(expected)) {
                showError(errorText + " ||| Byte " + i + ": Expected " + Arrays.toString(expected.toByteArray()) + ", got " + Arrays.toString(result.toByteArray()));
                return false;
            }
        }
        return true;
    }

    private static void showError(String errorText) {
        System.err.println(errorText);
    }

    private static void printPassed(String message) {
        if (printPassed) {
            System.out.println(message);
        }
    }
}
