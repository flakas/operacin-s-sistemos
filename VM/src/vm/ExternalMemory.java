package vm;

import java.util.Arrays;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class ExternalMemory {
    public static final int NUM_OF_BLOCKS = 100;
    public static final int NUM_OF_WORDS_PER_BLOCK = 16;
    public static final int WORD_SIZE = 4;

    private Word[] memory;
    private File filename;
    private RandomAccessFile harddrive;

    public ExternalMemory (File filename) {
        this.filename = filename;
        try {
            this.harddrive = new RandomAccessFile(filename, "rw"); // Read, write access
        } catch (FileNotFoundException ex) {
            throw new RuntimeException("Cannot find the harddrive file");
        }
    }

    public int blockIndexFromAddress(Word address) {
        int index = 0;
        int j = 0;
        int tempNum;

        int word = address.getByte(WORD_SIZE - 1) % NUM_OF_WORDS_PER_BLOCK;
        for (int i = WORD_SIZE - 1 - 1; i >= 0; i -= 1) {
            tempNum = address.getByte(i);
            for (int k = 0; k < j; k++) {
                tempNum *= RM.NUMERAL_SYSTEM;
            }
            index += tempNum;
            j++;
        }
        index = index * NUM_OF_WORDS_PER_BLOCK + (address.getByte(WORD_SIZE - 1) - word) / NUM_OF_WORDS_PER_BLOCK;
        return index;
    }

    public Word addressFromBlockIndex(int index) {
        if (index >= NUM_OF_BLOCKS) {
            throw new RuntimeException("Trying to address more blocks than external memory has");
        }
        short[] addressBytes = new short[WORD_SIZE];

        int smallBlock = index % NUM_OF_WORDS_PER_BLOCK;
        addressBytes[WORD_SIZE - 1] = (short)((smallBlock) * NUM_OF_WORDS_PER_BLOCK);
        index = (index - smallBlock) / NUM_OF_WORDS_PER_BLOCK;
        for (int i = WORD_SIZE - 1 - 1; i >= 0; i -= 1) {
            addressBytes[i] = (short)(index % RM.NUMERAL_SYSTEM);
            index = (index - addressBytes[i]) / RM.NUMERAL_SYSTEM;
        }

        Word address = new Word(addressBytes);
        return address;
    }

    public int wordIndexFromAddress(Word address) {
        int index = blockIndexFromAddress(address) * NUM_OF_WORDS_PER_BLOCK;
        index += address.getByte(WORD_SIZE - 1) % NUM_OF_WORDS_PER_BLOCK;
        return index;
    }

    public Word getWord(Word address) {
        int index = wordIndexFromAddress(address);

        Word result = new Word();
        try {
            this.harddrive.seek(index * (WORD_SIZE + 1));
            for(int i = 0; i <= WORD_SIZE - 1; i += 1) {
                result.setByte(i, (short)this.harddrive.read());
            }
            this.harddrive.read(); // reads /n character
        } catch (IOException ex) {
            System.out.println("Cannot getWord from harddrive");
            System.exit(1);
        }


        RM.getInstance().setIOI((byte)3);
        return result;
    }

    public boolean setWord(Word address, Word value) {
        int index = wordIndexFromAddress(address);

        try {
            this.harddrive.seek(index * (WORD_SIZE + 1));
            for(int i = 0; i <= WORD_SIZE - 1; i += 1) {
                this.harddrive.write((byte)value.getByte(i));
            }
            this.harddrive.write((byte)10);
        } catch (IOException ex) {
            System.out.println("Cannot setWord from harddrive");
            System.exit(1);
        }

        RM.getInstance().setIOI((byte)4);
        return true;
    }

}
