package vm;

import java.util.Arrays;

public class Memory {
    public static final int NUM_OF_BLOCKS = 5*16;
    public static final int NUM_OF_WORDS_PER_BLOCK = 16;
    public static final int WORD_SIZE = 4;
    public static final int BLOCK_STEP = 7;

    private Word[] memory;
    private boolean[] takenBlocks;

    public Memory () {
        this.memory = new Word[NUM_OF_BLOCKS * NUM_OF_WORDS_PER_BLOCK];
        this.takenBlocks = new boolean[NUM_OF_BLOCKS];

        for (int i = 0; i < this.memory.length; i += 1) {
            this.memory[i] = new Word();
        }
    }

    public Word getRealBlockAddress(Word vmAddress)
    throws InterruptException {
        int ptrIndex = this.blockIndexFromAddress(RM.getInstance().getPTR());
        short block = Word.getLeftNibble(vmAddress.getByte(3));
        return this.memory[ptrIndex * RM.HEX_SYSTEM + block];
    }

    public Word getBlock()
    throws InterruptException {
        int checked = 0;
        int current = 0;
        for(int i = 0; i < NUM_OF_BLOCKS; i += 1) {
            checked += 1;
            Word address = getBlock(current);
            if (address != null) {
                return address;
            }
            current = (current + BLOCK_STEP) % NUM_OF_BLOCKS;
        }
        throw new InterruptException((byte)4);
    }

    public Word getBlock(int index)
    throws InterruptException {
        if (index < 0 || index >= NUM_OF_BLOCKS) {
            throw new InterruptException((byte)3);
        }

        if (this.isBlockFree(index)) {
            this.takenBlocks[index] = true;
            return this.addressFromBlockIndex(index);
        }
        return null;
    }

    public boolean freeBlock(Word address)
    throws InterruptException {
        int index = this.blockIndexFromAddress(getRealBlockAddress(address));
        return this.freeBlock(index);
    }

    public boolean freeBlock(int index)
    throws InterruptException {
        if (index >= 0 && index < NUM_OF_BLOCKS) {
            this.takenBlocks[index] = false;
            return true;
        }
        throw new InterruptException((byte)3);
    }

    public boolean isBlockFree(int index) {
        return this.takenBlocks[index] == false;
    }

    public boolean isBlockFree(Word address)
    throws InterruptException {
        return this.isBlockFree(this.blockIndexFromAddress(address));
    }

    public int blockIndexFromAddress(Word address)
    throws InterruptException {
        int index = 0;

        for (int i = WORD_SIZE - 1 - 1; i >= 0; i -= 1) {
            if (i == WORD_SIZE - 1 - 1) {
                index += address.getByte(i);
            } else {
                index = (int)(index + (address.getByte(i) * Math.pow(RM.NUMERAL_SYSTEM, (WORD_SIZE - 1 - 1) - i)));
            }
        }
        index = index * RM.HEX_SYSTEM + Word.getLeftNibble(address.getByte(WORD_SIZE - 1));

        if (index < 0 || index >= NUM_OF_BLOCKS) {
            throw new InterruptException((byte)3);
        }
        return index;
    }

    public Word addressFromBlockIndex(int index)
    throws InterruptException {
        if (index < 0 || index >= NUM_OF_BLOCKS) {
            throw new InterruptException((byte)3);
        }
        short[] addressBytes = new short[WORD_SIZE];

        addressBytes[WORD_SIZE - 1] = (short)((index % RM.HEX_SYSTEM) << 4);
        index /= RM.HEX_SYSTEM;

        for (int i = WORD_SIZE - 1 - 1; i >= 0; i -= 1) {
            addressBytes[i] = (short)(index % RM.NUMERAL_SYSTEM);
            index /= RM.NUMERAL_SYSTEM;
        }

        Word address = new Word(addressBytes);
        return address;
    }

    public Word getWord(Word address)
    throws InterruptException {
        Word blockAddress = getRealBlockAddress(address);
        Word w = new Word(new short[]{0,0,0,Word.getRightNibble(address.getByte(3))});

        return getWordReal(RM.internalAdd(blockAddress, w));
    }

    public boolean setWord(Word address, Word value)
    throws InterruptException {
        Word blockAddress = getRealBlockAddress(address);
        Word w = new Word(new short[]{0,0,0,Word.getRightNibble(address.getByte(3))});

        return setWordReal(RM.internalAdd(blockAddress, w), value);
    }

    public Word getWordReal(Word address)
    throws InterruptException {
        int index = blockIndexFromAddress(address);

        if (!this.isBlockFree(index)) {
            int memIndex = index * NUM_OF_WORDS_PER_BLOCK + Word.getRightNibble(address.getByte(WORD_SIZE - 1));
            return memory[memIndex];
        }

        throw new InterruptException((byte)3);
    }

    public boolean setWordReal(Word address, Word value)
    throws InterruptException {
        int index = blockIndexFromAddress(address);

        if (this.isBlockFree(index)) {
            return false;
        }

        int memIndex = index * NUM_OF_WORDS_PER_BLOCK + Word.getRightNibble(address.getByte(WORD_SIZE - 1));
        memory[memIndex] = value;
        return true;
    }

    @Override
    public String toString() {
        String ret = "";
        for (int i = 0; i < memory.length; i += 1) {
            if (i % 4 == 0) {
                ret += String.format("%8x: ", i);
            }
            ret += memory[i] + "   ";
            if ((i+1) % 4 == 0) {
                ret += "\n";
            }
            if ((i+1) % 16 == 0) {
                ret += "\n";
            }
        }
        ret += "\n";
        return ret;
    }

}
