package vm;

import java.io.File;
import vm.gui.*;

public class RM {
    public static boolean DEBUG = true;
    public static int DEBUGLEVEL = 1;
    public static short NUMERAL_SYSTEM = 256;
    public static short HEX_SYSTEM = 16;
    public static byte NUMBER_OF_CS_BLOCKS = 5;
    public static byte NUMBER_OF_DS_BLOCKS = 5;
    public static byte NUMBER_OF_SS_BLOCKS = 5;
    public static byte NUMBER_OF_PT_BLOCKS = 1;
    public static byte NUMBER_OF_GS_BLOCKS = 16;
    public static byte BLOCKS_PER_VM = 16;
    public static File EXTERNAL_MEMORY_FILE = new File("harddrivesum.txt");
    public static byte CYCLES_PER_PROCESS = 100;

    private static RM instance;
    private Memory memory;
    private ExternalMemory externalMemory;
    private byte Mode;

    // Interrupt registers
    private byte PI;
    private byte SI;
    private byte TI = CYCLES_PER_PROCESS;
    private byte IOI;

    //Segment registers
    private Word GS = new Word();

    // Current VM
    private VM vm;

    // Current VM pages table register
    private Word PTR;

    // Hard drive file
    private File externalMemoryFileName = EXTERNAL_MEMORY_FILE;

    // Semaphores
    private byte internalSemaphore = 1;
    private byte externalSemaphore = 1;

    private RM () {
        this.memory = new Memory();
        this.externalMemory = new ExternalMemory(this.externalMemoryFileName);

        // Creating General Segment
        // Allocate NUMBER_OF_GS_BLOCKS blocks memory
        try {
            int startGS = Memory.NUM_OF_BLOCKS - NUMBER_OF_GS_BLOCKS;
            this.GS = this.memory.addressFromBlockIndex(startGS);
            for (int i = startGS; i < Memory.NUM_OF_BLOCKS; i++) {
                if (!this.memory.isBlockFree(i)) {
                    this.memory.freeBlock(this.memory.addressFromBlockIndex(i));
                }
                this.memory.getBlock(i);
            }
        }
        catch (InterruptException e) {
            System.out.println("Wrong params in code");
            e.printStackTrace();
            System.exit(1);
        }
    }

    private RM(File filename) {
        this();
        this.externalMemoryFileName = filename;
        this.externalMemory = new ExternalMemory(this.externalMemoryFileName);
    }

    public static RM getInstance() {
        return (instance == null)? instance = new RM() : instance;
    }

    public static RM getInstance(File filename) {
        return (instance == null) ? instance = new RM(filename) : instance;
    }

    public static void clearInstance() {
        instance = null;
    }

    public boolean step() {
        printDebug("[RM] New step", 1);
        this.resetInterrupts();
        this.vm.step();
        if (this.test()) {
            this.setMode((byte)1);
            // handle interrupts
            if (this.SI == 5) { // HALT
                return false;
            }
            if (this.TI <= 0) {
                System.out.println("TI interrupt");
                this.setTI(CYCLES_PER_PROCESS);
            }
            if (this.SI > 0) {
                Channel ch = null;
                switch (this.SI) {
                    case 1: ch = new InChannel();
                            break;
                    case 2: ch = new OutChannel();
                            break;
                    case 3:
                            try {
                                this.vm.pushStack(this.externalMemory.getWord(this.vm.getCurrentParam(1)));
                            } catch (InterruptException ex) {
                                // TODO: ignore any interrupts
                            }
                            break;
                    case 4:
                            this.externalMemory.setWord(this.vm.getCurrentParam(1), this.vm.getCurrentParam(2));
                            break;
                    case 6:
                            try {
                                this.vm.pushStack(internalAdd(this.GS, this.vm.getCurrentParam(1)));
                            } catch (InterruptException ex) {
                                // TODO: ignore any interrupts
                            }
                            break;
                    case 7:
                            try {
                                this.memory.setWord(internalAdd(this.GS, this.vm.getCurrentParam(1)), this.vm.getCurrentParam(2));
                            } catch (InterruptException ex) {
                                // TODO: ignore any interrupts
                            }
                            break;
                }
                if (ch != null) {
                    ch.exchange();
                }
            }
            if (this.PI > 0) {
                switch (this.PI) {
                    case 1: System.out.println("[INTERRUPT] Unrecognized command" + this.vm.getCurrentParam(1));
                            return false;
                    case 2: System.out.println("[INTERRUPT] Divide by zero");
                            return false;
                    case 3: System.out.println("[INTERRUPT] Wrong address");
                            return false;
                    case 4: System.out.println("[INTERRUPT] Out of memory");
                            return false;
                    case 5: System.out.println("[INTERRUPT] You can't use this command here");
                            return false;
                }
            }
        }

        return true;

    }

    public boolean run() {
        printDebug("************ The Real Machine! ************");
        printDebug("[RM]Creating a virtual machine...");
        printDebug("[RM]Running...");
        boolean s = true;
        while(this.SI != 5 && s) {
            s = this.step();
        }
        printDebug("[RM]Stopping...");
        return true;
    }

    public boolean createVM() {
        printDebug("[RM]Creating VM");
        // Allocate BLOCKS_PER_VM blocks of memory for VM
        Word[] pagesTable = new Word[BLOCKS_PER_VM];
        try {
            for(int i = 0; i < BLOCKS_PER_VM; i += 1) {
                pagesTable[i] = this.memory.getBlock();
            }
        }
        catch (InterruptException e) {
            System.out.println("Wrong params in code");
        }

        // Initialize registers
        Word cs = new Word(new short[] {0, 0, 0, 0}),
             ds = new Word(new short[] {0, 0, 0, (short)(NUMBER_OF_CS_BLOCKS * RM.HEX_SYSTEM)}),
             ss = new Word(new short[] {0, 0, 0, (short)((NUMBER_OF_CS_BLOCKS + NUMBER_OF_DS_BLOCKS) * RM.HEX_SYSTEM)}),
             pt = new Word(new short[] {0, 0, 0, (short)((NUMBER_OF_CS_BLOCKS + NUMBER_OF_DS_BLOCKS + NUMBER_OF_SS_BLOCKS) * RM.HEX_SYSTEM)}),
             sp = new Word(new short[] {0, 0, 0, (short)((NUMBER_OF_SS_BLOCKS - 1) * RM.HEX_SYSTEM + (short)(Memory.WORD_SIZE - 1))});

        this.PTR = pagesTable[BLOCKS_PER_VM - 1].copy();

        // Load Page Table with addresses
        try {
            Word currentWord = pagesTable[NUMBER_OF_CS_BLOCKS + NUMBER_OF_DS_BLOCKS + NUMBER_OF_SS_BLOCKS].copy();
            for (int i = 0; i < BLOCKS_PER_VM; i += 1) {
                this.memory.setWordReal(currentWord, pagesTable[i]);
                currentWord = RM.internalAdd(currentWord, Word.ONE);
            }
        }
        catch (InterruptException e) {
            System.out.println("Wrong params in code");
        }


        // Init VM
        this.vm = new VM(cs, ds, ss, sp);

        // Load a program into VM memory
        //this.loadProgramToVM(new Word(new short[] {0, 0, 0, 0}));

        return true;
    }

    public VM getCurrentVM() {
        return this.vm;
    }

    public Memory getMemory() {
        return this.memory;
    }

    public ExternalMemory getExternalMemory() {
        return this.externalMemory;
    }

    public void loadProgramToVM(Word startAddress) {
        printDebug("[RM]Loading program to VM");
        Word w = this.externalMemory.getWord(startAddress);

        Word vmShift = new Word();

        Word eaddress = startAddress.copy();
        int wordCounter = 0;

        try {
            // Move data to DS
            if ((new Word(new short[] {'D', 'A', 'T', 'A'})).equals(w)) {
                Word code = new Word(new short[] {'C', 'O', 'D', 'E'});
                Word db = new Word(new short[] {'D', 'B', ' ', ' '});
                Word dw = new Word(new short[] {'D', 'W', ' ', ' '});
                eaddress = internalAdd(eaddress, Word.ONE);
                w = externalMemory.getWord(eaddress);
                do {
                    if (wordCounter + 1 > NUMBER_OF_DS_BLOCKS * Memory.NUM_OF_WORDS_PER_BLOCK) {
                        throw new RuntimeException("Wrong program file structure");
                    }
                    if(w.equals(db)) {
                        wordCounter += 1;
                        eaddress = internalAdd(eaddress, Word.ONE);
                        w = externalMemory.getWord(eaddress);
                        this.vm.setVMWord((short)1, vmShift, w);
                        printDebug("[VM]setVMWord(1, address: " + vmShift + ", value: " + w, 2);
                        vmShift = internalAdd(vmShift, Word.ONE);
                    } else if (w.equals(dw)) {
                        wordCounter += 1;
                        eaddress = internalAdd(eaddress, Word.ONE);
                        w = externalMemory.getWord(eaddress);
                        eaddress = internalAdd(eaddress, Word.ONE);
                        Word w2 = externalMemory.getWord(eaddress);
                        //for (int i = 0; i < Memory.WORD_SIZE; i += 1) {
                            //if (w.getByte(i) >= 65) {
                                //w.setByte(i, (short)(w.getByte(i) - 65 + 10)); // 65 = A (ASCII)
                            //} else {
                                //w.setByte(i, (short)(w.getByte(i) - 48)); // 48 = 0 (ASCII)
                            //}
                        //}
                        w = this.vm.paramToAddressTwoWords(w2, w);
                        //w = this.vm.paramToAddress(w);
                        this.vm.setVMWord((short)1, vmShift, w);
                        printDebug("[VM]setVMWord(1, address: " + vmShift + ", value: " + w, 2);
                        vmShift = internalAdd(vmShift, Word.ONE);
                    } else if (w.equals(code)) {
                        break;
                    } else {
                        throw new RuntimeException("Wrong program file structure ");
                    }
                    eaddress = internalAdd(eaddress, Word.ONE);
                    w = externalMemory.getWord(eaddress);
                } while (w.equals(code) != true);
            } else {
                throw new RuntimeException("Wrong program file structure");
            }
        }
        catch (InterruptException e) {
            System.out.println("Error moving data to DS");
        }

        wordCounter = 0;

        try {
            // Move code to CS
            if ((new Word(new short[] {'C', 'O', 'D', 'E'})).equals(w)) {
                Word halt = new Word(new short[] {'H', 'A', 'L', 'T'});
                vmShift = new Word();
                do {
                    wordCounter += 1;
                    if (wordCounter > NUMBER_OF_CS_BLOCKS * Memory.NUM_OF_WORDS_PER_BLOCK) {
                        throw new RuntimeException("Wrong program file structure");
                    }
                    eaddress = internalAdd(eaddress, Word.ONE);
                    w = externalMemory.getWord(eaddress);
                    this.vm.setVMWord((short)0, vmShift, w);
                    printDebug("[VM]setVMWord(0, address: " + vmShift + ", value: " + w, 2);
                    vmShift = internalAdd(vmShift, Word.ONE);
                } while (w.equals(halt) != true);
            } else {
                throw new RuntimeException("Wrong program file structure");
            }
        }
        catch (InterruptException e) {
            System.out.println("Error moving code to CS");
        }
    }


    public static Word internalAdd(Word op1, Word op2) {
        Word result = new Word();
        short sum;
        short carry = 0;

        for (int i = Memory.WORD_SIZE - 1; i >= 0; i -= 1) {
            if ((sum = (short)(op1.getByte(i) + op2.getByte(i) + carry)) >= NUMERAL_SYSTEM) {
                carry = 1;
                sum %= NUMERAL_SYSTEM;
            }
            else {
                carry = 0;
            }
            result.setByte(i, sum);
        }
        return result;
    }

    public static Word internalSub(Word op1, Word op2) {
        Word result = new Word();
        short diff;
        short borrow = 0;

        for (int i = Memory.WORD_SIZE - 1; i >= 0; i -= 1) {
            if ((diff = (short)(op1.getByte(i) - op2.getByte(i) - borrow)) < 0) {
                borrow = 1;
                diff += NUMERAL_SYSTEM;
            }
            else {
                borrow = 0;
            }
            result.setByte(i, diff);
        }
        return result;
    }


    // type: 1 - external; 2 - internal
    public boolean acquireSemaphore(short type) {
        switch (type) {
            case 1:
                if (externalSemaphore > 0) {
                    externalSemaphore -= 1;
                    return true;
                }
                break;
            case 2:
                if (internalSemaphore > 0) {
                    internalSemaphore -= 1;
                    return true;
                }
                break;
        }
        return false;
    }

    // type: 1 - external; 2 - internal
    public boolean getSemaphore(short type) {
        switch (type) {
            case 1:
                return externalSemaphore > 0;
            case 2:
                return internalSemaphore > 0;
        }
        return false;
    }

    // type: 1 - external; 2 - internal
    public void releaseSemaphore(short type) {
        switch (type) {
            case 1:
                if (externalSemaphore == 0) {
                    externalSemaphore += 1;
                }
                break;
            case 2:
                if (internalSemaphore == 0) {
                    internalSemaphore += 1;
                }
                break;
        }
    }

    public void resetInterrupts() {
        this.setPI((byte)0);
        this.setSI((byte)0);
        this.setIOI((byte)0);
        this.setMode((byte)0);
    }

    public boolean test() {
        return this.TI <= 0 || this.PI != 0 || this.SI != 0 || this.IOI != 0;
    }

    // Register setters and getters

    public byte getPI() {
        return this.PI;
    }

    public void setPI(byte value) {
        this.PI = value;
    }

    public Word getGS() {
        return this.GS;
    }

    public void setGS(Word GS) {
        this.GS = GS;
    }

    public byte getMode() {
        return this.Mode;
    }

    public void setMode(byte mode) {
        this.Mode = mode;
    }

    public byte getSI() {
        return this.SI;
    }

    public void setSI(byte SI) {
        this.SI = SI;
    }

    public byte getTI() {
        return this.TI;
    }

    public void setTI(byte TI) {
        this.TI = TI;
    }

    public byte getIOI() {
        return this.IOI;
    }

    public void setIOI(byte IOI) {
        this.IOI = IOI;
    }

    public Word getPTR() {
        return this.PTR;
    }

    public void setPTR(Word PTR) {
        this.PTR = PTR;
    }

    public void decrementTI(byte x) {
        if (this.Mode != 1) {
            this.setTI((byte)(this.getTI() - x));
        }
    }

    // Debugging

    public static void printDebug(String text) {
        if (DEBUG) {
            System.out.println(text);
        }
    }

    public static void printDebug(String text, int level) {
        if (DEBUGLEVEL <= level) {
            printDebug(text);
        }
    }
}
