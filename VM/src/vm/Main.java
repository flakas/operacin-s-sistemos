import vm.*;
import vm.gui.*;

public class Main {

    public static void main(String[] args) {
        if (args.length > 0 && args[0].equals("nogui")){
            RM.getInstance().createVM();
            RM.getInstance().loadProgramToVM(new Word(new short[] {0, 0, 0, 0}));
            RM.getInstance().run();
        } else {
            Window.getInstance().makeVisible();
        }

        //System.out.println(RM.getInstance().getMemory());
    }

}
